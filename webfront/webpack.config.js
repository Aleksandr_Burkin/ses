const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const postcssImport = require('postcss-import');

require('babel-polyfill').default;

const PATHS = {
    app: path.join(__dirname, './src')
};

var config = {
    devtool: 'cheap-module-eval-source-map',

    entry: [
        'babel-polyfill',
        'bootstrap-loader',
        PATHS.app
    ],
    
    output: {
        path: __dirname,
        filename: './static/bundle.js'
    },

    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss'],
        modules: ['node_modules', PATHS.app]
    },

    devServer: {
        historyApiFallback: true, //F5
        inline: true,
        port: 9000
    },
    
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    plugins: ['transform-decorators-legacy'],
                    presets: ['es2015', 'stage-0', 'react']
                }
            },
            {
                test: /bootstrap-sass\/assets\/javascripts\//,
                loader: 'imports-loader'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            },
            {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            },
            {
                test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader'
            },
            {
                test: /\.png$/,
                loader: 'file-loader'
            },
            {
                test: /\.jpg$/,
                loader: 'file-loader'
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
            }
        ]
    },

    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: function (webpack) {
                    return [
                        postcssImport({
                            addDependencyTo: webpack
                        }),
                        autoprefixer
                    ]
                }
            }
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            jQuery: 'jquery'
        })
    ]
};

module.exports = config;