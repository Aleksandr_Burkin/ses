(function() {
	$('button.add-deal').bind('click', function() {
		$('div.panel-deal').css('display', 'block');
	});

	$('button.ok').bind('click', function() {
		$('div.panel-deal').css('display', 'none');
	});

	$('button.cancel').bind('click', function() {
		$('div.panel-deal').css('display', 'none');
	});
})();