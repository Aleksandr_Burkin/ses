import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import SES from './SES.jsx';
import LoginPage from './component/LoginPage.jsx';
import Businessman from './component/Businessman.jsx';
import SearchCompany from './component/SearchCompany.jsx';
import Company from './component/Company.jsx';
import Money from './component/general/Money.jsx';


/*
    //Как передать параметр в компонент (например paramName)
    <Route path='businessman/:paramName' component={Businessman}/>
    <Route path='searchCompany' component={SearchCompany} anyParamName={paramName}/>

    //Получить в компоненте SearchCompany можно так
    this.props.route.businessmanId

    this.props.params.businessmanId
*/

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path='/' component={SES}>
            <IndexRoute component={SES} />
            <Route path='login' component={LoginPage}/>
            <Route path='businessman' component={Businessman}/>
            <Route path='searchCompany' component={SearchCompany}/>
            <Route path='company/:inn' component={Company}/>
        </Route>
    </Router>,
    document.getElementById('app')
);