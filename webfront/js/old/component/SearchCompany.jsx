import React, {Component} from 'react';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import AlertContainer from 'react-alert';
import Money from './general/Money.jsx';

class SearchCompany extends Component {

    constructor(props) {
        super(props);
        this.state = {companies: []};
    }

    componentDidMount() {
        let url = '/freeCompanies';
        performSecureRequest(url, 'GET', (function(data) {
            this.setState({companies: data});
        }).bind(this));
    }

    render() {
        return (
            <div>
                <CompanyList companies={this.state.companies}/>
            </div>
        )
    }
}

class CompanyList extends Component {
	render() {
		var companies = this.props.companies.map(company =>
			<Company key={company.inn} company={company}/>
		);
		return (
			<div className="companies">
				<div className="title">Поиск компаний</div>

                <table>
                    <tbody>
                        {companies}
                    </tbody>
                </table>
			</div>
		)
	}
}

class Company extends Component {

    constructor(props) {
        super(props);
        this.buyCompany = this.buyCompany.bind(this);
        this.alertOptions = {
            offset: 14,
            position: 'bottom left',
            theme: 'dark',
            time: 5000,
            transition: 'scale'
        };
    }

    showAlert(){
        msg.show('У Вас недостаточно денег', {
            time: 3000,
            type: 'success'
        });
    }

    buyCompany() {
        let url = '/buyCompany/' + this.props.company.inn;
        let errorFunction = function() {
            //this.showAlert();
            error('error buy company');
        };
        performSecureRequest(url, 'PUT', (function() {
            log('success buy company');
            browserHistory.push('/businessman');
        }).bind(this), errorFunction);
    }

	render() {
		return (
			<tr>
				<td> <img className="logo" src={this.props.company.logo}/> </td>
				<td>
					<div className="name">{this.props.company.name}</div>
					<div className="economicType"> {this.props.company.economicType.description} </div>
					<div className="address"> {this.props.company.address} </div>
                    <Money value={this.props.company.price.value} currency={this.props.company.price.currency} />
				</td>
				<td>
				    <AlertContainer ref={(a) => global.msg = a} {...this.alertOptions} />
                    <button className="buy" onClick={this.buyCompany}>
                        Приобрести
                    </button>
				</td>
			</tr>
		)
	}
}

export default SearchCompany;