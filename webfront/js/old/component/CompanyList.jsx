import React, {Component} from 'react';
import { Link } from 'react-router';
import Money from './general/Money.jsx';

class CompanyList extends Component {
    render() {
        var companies = this.props.companies.map(company =>
            <Company key={company.inn} company={company}/>
        );
        return (
            <div className="companies">
                <div className="title">Компании</div>

                { this.props.companies.length != 0 ?
                    <table>
                        <tbody>
                        {companies}
                        </tbody>
                    </table>
                    :
                    <div>
                        На данный момент Вы не владеете предприятиями.
                        Вы можете <Link to='searchCompany'>приобрести</Link> компанию и
                        начать строить Ваш собственный бизнес.
                    </div>
                }
            </div>
        )
    }
}

class Company extends Component {
    render() {
        return (
            <tr>
                <td> <img className="logo" src={this.props.company.logo}/> </td>
                <td>
                    <Link to={'company/' + this.props.company.inn}> {this.props.company.name} </Link><br/>
                    <Money value={"99999"} currency={"RUR"} />
                </td>
            </tr>
        )
    }
}

export default CompanyList;