import React, {Component} from 'react';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';

class LoginPage extends Component {

    login() {
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;

        var allHeaders = new Headers();
        allHeaders.append('X-Requested-With', 'XMLHttpRequest');
        allHeaders.append('Content-Type', 'application/json');
        allHeaders.append('Cache-Control', 'no-cache');

        var payload = {
            email: email,
            password: password
        };

        var reqInit = {
            method: 'POST',
            headers: allHeaders,
            body: JSON.stringify(payload)
        };

        fetch('http://localhost:9090/api/auth/login', reqInit)
            .then(function(response) {
                if (response.status !== 200) {
                    log('Error authentication');
                    return;
                }
                response.json().then(function(data) {
                    //log(JSON.stringify(data));
                    localStorage.setItem('SeS_token', JSON.stringify(data));
                    browserHistory.push('/businessman');
                });
            });
    }

    exampleRequestWithToken() {
        let token = localStorage.getItem('SeS_token');

        var allHeaders = new Headers();
        allHeaders.append('X-Authorization', 'Bearer ' + JSON.parse(token).token);
        allHeaders.append('Cache-Control', 'no-cache');

        var reqInit = {
            method: 'GET',
            headers: allHeaders
            //credentials: 'include'
        };

        fetch('http://localhost:9090/api/user', reqInit)
            .then(function(response) {
                if (response.status !== 200) {
                    log('Error authentication');
                    return;
                }
                response.json().then(function(data) {
                    log(JSON.stringify(data));
                });
            });
    }

    render() {
        return (
            <div>
                <span className="greeting"> Сэc приветствует Вас, бизнесмен! </span> <br/>
                <div className="loginForm">
                    <input id="email" type="text" placeholder="Ваш e-mail..." defaultValue="bay-burkin@yandex.ru"/> <br/>
                    <input id="password" type="password" placeholder="Ваш пароль..." defaultValue="test1234"/> <br/>
                    <button className="login" onClick={this.login.bind(this)}>Войти</button>
                </div>
            </div>
        )
    }
}

export default LoginPage;
