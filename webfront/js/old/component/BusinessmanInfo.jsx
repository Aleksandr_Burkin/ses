import React, {Component} from 'react';
import { Link } from 'react-router';
import Money from './general/Money.jsx';

class BusinessmanInfo extends Component {

    render() {
        return (
            <div>
                <div>
                    <img className="avatar" src={this.props.businessman.avatar}/>
                    <div className="user_info">
                        <div> <b>Ранг:</b> 1 </div>
                        <div> <b>Рeйтинг:</b> 666 </div>

                        <div className="action">
                            <Link to='searchCompany'> Приобрести </Link><br/>
                            <a href=""> Партнеры </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BusinessmanInfo;
