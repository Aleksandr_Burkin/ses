import React, {Component} from 'react';
import { Link } from 'react-router';
import Money from './general/Money.jsx';
import BusinessmanInfo from './BusinessmanInfo.jsx';
import CompanyList from './CompanyList.jsx';

class Businessman extends Component {

    constructor(props) {
        super(props);
    	this.state = {
            businessman: {},
            companies: []
        };
    }

    componentDidMount() {
        let url = '/businessmanInfo';
        performSecureRequest(url, 'GET', (function(data) {
            this.setState({businessman: data, companies: data.companies});
            this.props.refreshMoney(data.money.value);
        }).bind(this));
    }

    render() {
        return (
            <div>
                <div className="main">
                    <div className="user">
                        <div className="businessman"> {this.state.businessman.firstName} {this.state.businessman.lastName} </div>
                    </div>
                    <BusinessmanInfo businessman={this.state.businessman} />
                    <CompanyList companies={this.state.companies}/>
                </div>
                <div className="ribbon">
                    <div className="title"> Лента событий </div>
                    <hr/>
                    <div className="event"> Вы получили начальный капитал размером в 100 000 &#8381;. <br/>
                    Удачного бизнеса!
                    </div>
                    <hr/>
                </div>
            </div>
        )
    }
}

export default Businessman;
