import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import Money from './general/Money.jsx';
import Field from './general/Field.jsx';
import { browserHistory } from 'react-router';
import update from 'react-addons-update';

class Company extends  Component {

    constructor() {
        super(...arguments);
        this.state = {
            company: {
                name: '',
                economicType: {
                    description: ''
                },
                price: {
                    currency: '',
                    value: 0
                },
                asset: {
                    currency: '',
                    value: 0
                },
                address: ''
            }
        };
    }

    fetchData(inn) {
        let url = '/company/' + inn;
        performSecureRequest(url, 'GET', (function(data) {
            this.setState({
                company: data,
                asset: data.asset
            });
        }).bind(this));
    }

    componentDidMount() {
        this.fetchData(this.props.params.inn);
        var elems = document.getElementsByClassName('moneyInput');
        for (var i = 0; i < elems.length; i++) {
            CBS.addContent(elems[i], new RegExp(/^-?\d*$/), 'amount', true);
        }
    }

    invest() {
        var investment = ReactDOM.findDOMNode(this.refs.investmentField).value.replace(/\s/g, '');
        var url = '/investToCompany/' + investment;
        performSecureRequest(url, 'PUT', (function(data) {
            this.setState({
                company: update(this.state.company, {asset: {value: {$set: data.asset}}})
            });
            this.props.refreshMoney(data.money);
        }).bind(this));

        url = '/compatibleCompanies';
        performSecureRequest(url, 'GET', (function(data) {
            log(data);
        }).bind(this));
    }

    render() {
        return (
            <div className="company">
                <div className="name"> {this.state.company.name} </div>
                <div className="economicType"> {this.state.company.economicType.description} </div>
                <table>
                    <tbody>
                        <tr className="info">
                            <td>
                                <img className="logo" src={this.state.company.logo}/>
                            </td>
                            <td>
                                <Money title={"Капитал"}
                                       value={this.state.company.asset.value}
                                       currency={this.state.company.asset.currency} />
                                <hr/>
                                <Money title={"Общий доход"}
                                       value={"0"}
                                       currency={"RUR"} />
                                <Money title={"Валовая прибыль"}
                                       value={"0"}
                                       currency={"RUR"} />
                                <Money title={"Накладные расходы"}
                                       value={"0"}
                                       currency={"RUR"} />
                                <Money title={"Операционная прибыль"}
                                       value={"0"}
                                       currency={"RUR"} />
                                <Money title={"Чистая прибыль"}
                                       value={"0"}
                                       currency={"RUR"} />
                                <Field title={"Рентабельность"}
                                       value={"50%"} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr/>
                <input ref="investmentField" type="text" size="18" className="moneyInput"/>
                <button onClick={this.invest.bind(this)}>Инвестировать</button>
            </div>
        )
    }
}

export default Company;