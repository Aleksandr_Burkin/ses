import React, {Component, PropTypes} from 'react';

class Money extends Component {

    format(value) {
        return (value + "").split("").reverse().join("")
                           .replace(/(\d{3})/g, "$1 ").split("")
                           .reverse().join("").replace(/^ /, "");
    }

    render() {
        var title = this.props.title;
        return (
            <div className="money">
                {typeof title !== "undefined" ? <b> {title}: </b> : ''}
                <span> {this.format(this.props.value)} &#8381; </span>
            </div>
        )
    }
}

/*Money.propTypes = {
    value: PropTypes.number,
    currency: PropTypes.string
};*/

export default Money;