import React, {Component, PropTypes} from 'react';

class Field extends Component {

    render() {
        return (
            <div>
                <b> {this.props.title}: </b>
                <span> {this.props.value} </span>
            </div>
        )
    }
}

export default Field;