import React, { Component } from 'react';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import Money from './component/general/Money.jsx';
import Navbar from './component/Navbar.jsx';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {money: 0}
    }

    refreshMoney(value) {
        this.setState({money: value});
    }

	componentDidMount() {
        browserHistory.push('/login');
    }

    render() {
        const children = React.Children.map(this.props.children,
            (child) => React.cloneElement(child, {
                refreshMoney: this.refreshMoney.bind(this)
            })
        );

        return (
            <div className="middleDisplay">
                <div className="header">
                    <a className="to_main_page" href="/"/>
                    <Money value={this.state.money} currency={"RUR"} />
                </div>
                <hr/>
                {children}
            </div>
        )
    }
}