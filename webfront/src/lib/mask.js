/**
* mask.js v1.3
* A simple mask plugin
*
* Copyright 2016, Burkin Aleksandr
*/

(function() {
    var errorColor = '#E53D00';
    var defaultColor = '#292626';
    var separators = ['+', '-', '(', ')', '\'', '.', ':', '7', ' '];
    var space = '_';
    var seven = '7'; //for telephone
    var BACKSPACE = 8,
        SHIFT = 16,
        END = 35,
        HOME = 36,
        LEFT_ARROW = 37,
        TOP_ARROW = 38,
        RIGHT_ARROW = 39,
        DOWN_ARROW = 40,
        DELETE = 46;

    String.prototype.replaceChar = function(index, newChar) {
        if( !(newChar instanceof String) && newChar.length > 1 )
            throw new Error('Incorrect symbol');

        return this.slice(0, index) + newChar +
            this.slice(index + 1, this.length);
    };
    String.prototype.insertChar = function(index, newChar) {
        if( !(newChar instanceof String) && newChar.length > 1 )
            throw new Error('Incorrect symbol');

        return this.slice(0, index) + newChar +
            this.slice(index, this.length);
    };

    window.CBS = {};

    CBS.addMask = function(field, pattern, className) {
        if( field.maskName )
            if( field.maskName == className) return;
            else deleteMask(field);
        
        field.mask = true;
        field.maxLength = pattern.length;
        field.style.color = defaultColor;
        field.template = defineTemplate(pattern);
		field.startPrint = field.template.indexOf(space);
        field.maskName = className;
        field.placeholder = field.template;

        field.onkeypress = function(event) {
            var caretStart = getCaretStart(pattern, this.selectionStart);
            var character = String.fromCharCode(event.charCode);

            if( isCorrectSymbol(pattern, character, caretStart) ) {
                if( isDate(pattern) && !isCorrectDigitDate(this.value, character, caretStart) ) {
                    event.preventDefault();
                } else {
                    this.value = this.value.substring(0, this.selectionStart) +
                                 this.template.substring(this.selectionStart, this.selectionEnd) +
                                 this.value.substring(this.selectionEnd, this.value.length);
					this.selectionStart = caretStart;
					this.selectionEnd = caretStart + 1;
				}
            } else if( event.charCode ) //for Mozilla
                event.preventDefault();
        };
        var isShift = false;
        field.onkeydown = function(event) {
            if( (event.keyCode == HOME || event.keyCode == TOP_ARROW) && !isShift ) {
                defineCaretPosition(field, this.startPrint);
                event.preventDefault();
            } else if( event.keyCode == LEFT_ARROW ) {
                if( this.startPrint == this.selectionStart )
                    event.preventDefault();
            } else if( event.keyCode == BACKSPACE ) {
                if( this.selectionStart != this.selectionEnd ) {
                    var curSelectionStart = this.selectionStart;
                    this.value = this.value.substring(0, this.selectionStart) +
                                 this.template.substring(this.selectionStart, this.selectionEnd) +
                                 this.value.substring(this.selectionEnd, this.value.length);
                    if( this.startPrint >= curSelectionStart) this.selectionStart = this.startPrint + 1;
                    else this.selectionStart = curSelectionStart + 1;
                }

                var caretStart = getCaretStart(pattern, this.selectionStart - 1, -1);
                var character = this.value.charAt(caretStart);
                if( field.startPrint == this.selectionStart ) {
                    event.preventDefault();
                } else if( character && (!isSeparator(character) || character == seven) ) {
                    this.value = this.value.insertChar(caretStart + 1, space);
                    this.selectionEnd = caretStart + 1;
                }
            } else if( event.keyCode == DELETE ) {
                if( this.selectionStart != this.selectionEnd ) {
                    var curSelectionStart = this.selectionStart;
                    this.value = this.value.substring(0, this.selectionStart) +
                                 this.template.substring(this.selectionStart, this.selectionEnd) +
                                 this.value.substring(this.selectionEnd, this.value.length);
                    if( this.startPrint >= curSelectionStart) this.selectionStart = this.startPrint;
                    else this.selectionStart = curSelectionStart;
                }
                var caretStart = getCaretStart(pattern, this.selectionStart);
                var character = this.value.charAt(caretStart);
                if( !isSeparator(character) ) {
                    this.value = this.value.insertChar(caretStart + 1, space);
					this.selectionStart = caretStart;
                    this.selectionEnd = caretStart + 1;
                }
            } else if( event.keyCode == SHIFT ) {
                isShift = true;
            }
        };

        field.onkeyup = function(event) {
            if( event.keyCode == SHIFT ) isShift = false;
        }

        if( isDate(pattern) ) {
            field.onfocus = focusEventListener;
            field.onblur = blurEventListener;
        } else {
            bindEvent(field, 'focus', focusEventListener);
            bindEvent(field, 'blur', blurEventListener);
        }
        function focusEventListener() {
            if( !this.value ) this.value = this.template;
            defineCaretPosition(field, this.value.indexOf(space));
            this.style.color = defaultColor;
        }
        function blurEventListener() {
            if( this.value == this.template ) this.value = '';
            else
                if( isCorrectValue(field, pattern) ) this.style.color = defaultColor;
                else this.style.color = errorColor;
        }

        field.removeAllEventListeners = function() {
            unbindEvent(field, 'blur', blurEventListener);
            unbindEvent(field, 'focus', focusEventListener);
            var listeners = ['onkeypress','onkeydown','onmousedown','onkeyup','onfocus'];
            for(var i = 0; i < listeners.length; i++)
                field[listeners[i]] = undefined;
        };
    };

    function isDate(pattern) {
        return pattern.indexOf('99.99.9999') == 0;
    }
    function isCorrectValue(field, pattern) {
        if( field.value != field.template )
            if( (isDate(pattern) && !isCorrectDateTime(field.value)) ||
                (field.value.indexOf(space) != -1) )
                return false;

        return true;
    }
    function defineTemplate(pattern) {
        var template = '';
        for(var i = 0; i < pattern.length; i++) {
            var symbol = pattern.charAt(i);
            if( isSeparator(symbol) ) template += symbol;
            else template += space;
        }
        return template;
    }
    function defineCaretPosition(field, pos) {
        pos = (pos == -1) ? field.maxLength : pos;
        setTimeout(function() {
            field.selectionStart = pos;
            field.selectionEnd = pos;
        }, 0);
    }
    function fillMaskField(field) {
        if( field.value && field.value.length != field.template.length ) {
            var temp = '';
            for(var i = 0, j = 0; i < field.template.length; i++)
                if( field.template.charAt(i) == '_' )
                    temp += field.value.charAt(j++);
                else
                    temp += field.template.charAt(i);

            field.value = temp;
        } else if( !field.value ) {
            field.value = field.template;
        }
    }
    function isSeparator(character) {
        return separators.indexOf(character) != -1;
    }
    function getCaretStart(pattern, caret, step) {
        step = step || 1;
        for(var i = caret; i < pattern.length; i += step)
            if( !isSeparator(pattern.charAt(i)) )
                return i;
    }
    function isCorrectSymbol(pattern, symbol, position) {
        if( position === undefined ) return false;
        switch( pattern.charAt(position) ) {
            case 'A':
                return /[A-Z]/.test(symbol);
            case 'a':
                return /[a-z]/.test(symbol);
            case 'R':
                return /[А-Я]/.test(symbol);
            case 'r':
                return /[а-я]/.test(symbol);
            case '9':
                return /\d/.test(symbol);
            case '#':
                return /[A-Za-z0-9]/.test(symbol);
            case 'X':
                return true;
        }
        return false;
    }
	var isCorrectDateTime = (function() {
        var dateRegStr = '^(((0[1-9]|[12]\\d|3[01]).(0[13578]|1[02])' +
            '.((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)' +
            '.(0[13456789]|1[012]).((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])' +
            '.02.((19|[2-9]\\d)\\d{2}))|(29.02.((1[6-9]|[2-9]\\d)' +
            '(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$';
        var dateReg = new RegExp(dateRegStr);

        var timeRegStr = '(([0-1][0-9]|2[0-3]):([0-5][0-9])){1}((:[0-5][0-9])?)$';
        var timeReg = new RegExp(timeRegStr);

        return function(value) {
            var date = value.split(' ')[0];
            var time = value.split(' ')[1];

            if( time ) 
				return dateReg.test(date) && timeReg.test(time);
			else 
				return dateReg.test(date);
        };
    })();
	var isCorrectDigitDate = (function() {
        var reg = new RegExp('^((([0-2]|_)(\\d|_))|((3|_)([0-1]|_)))' +
                             '.(((0|_)(\\d|_))|((1|_)([0-2]|_))).(([0-9]|_){4})' +
                             '( ((([0-1]|_)(\\d|_))|((2|_)([0-3]|_)))' +
                             '(:(([0-5]|_)(\\d|_))){1,2}){0,1}$');

        return function(value, digit, caretStart) {
            var s = value.replaceChar(caretStart, digit);
            return reg.test(s);
        };
    })();
	
    CBS.addContent = function(field, regexp, className, isAmount) {
        if( field.maskName )
            if( field.maskName == className) return;
            else deleteMask(field);

        field.isContent = true;
        field.style.color = defaultColor;
        field.maskName = className;

        if( field.className.indexOf('amount') != -1 || isAmount ) { 
            field.isAmount = true;
            field.style.textAlign = 'right';
        }
        var previousValue = field.value;
        if( regexp.source == '^-?\\d*$' || field.isAmount ) {
            function inputEventListener() {
                if( !regexp.test(field.value.replace(/\u00A0/g, '')) )
                    field.value = previousValue;
                previousValue = field.value;

                var caretEnd = this.selectionEnd,
                    prevLength = this.value.length;
                var splitter = getSplitter(field);
                var parts = field.value.replace(/\u00A0/g, '').split(splitter);
                field.value = divideIntegerRank(parts[0]);

                if( field.isAmount && parts[1] !== undefined )
                    field.value += splitter.source.charAt(splitter.source.length - 1) + parts[1];

                this.selectionEnd = caretEnd + (this.value.length - prevLength);
            }
            bindEvent(field, 'input', inputEventListener);
        } else {
            function pasteEventListener() {
                if( !regexp.test(field.value.replace(/\u00A0/g, '')) )
                    field.value = previousValue;
                previousValue = field.value;
            }
            bindEvent(field, 'input', pasteEventListener);
        }

        field.onkeypress = function(event) {
            var key = event.charCode || event.keyCode;
            var symbol = String.fromCharCode(key);

            if( event.charCode ) { //for Mozilla
                var str = this.value.substring(0, this.selectionStart) + symbol +
                              this.value.substring(this.selectionEnd, this.value.length);
                if( !regexp.test(str.replace(/\u00A0/g, '')) )
                    event.preventDefault();
            }
        };

        field.removeAllEventListeners = function() {
            unbindEvent(field, 'input', inputEventListener);
            unbindEvent(field, 'input', pasteEventListener);
            unbindEvent(field, 'keydown', keydownEventListener);
            var listeners = ['onmousedown','onkeyup','onkeypress'];
            for(var i = 0; i < listeners.length; i++)
                field[listeners[i]] = undefined;
        };
	};

    function divideIntegerRank(value) {
        var negative = false;
        if( value.indexOf('-') != -1 ) {
            negative = true;
            value = value.substring(1, value.length);
        }

        var index = value.length % 3;
        var a = value.slice(index, value.length).match(/(\d){3}/g) || [];
        forEach(a, function(i) {
            if( !i && !index ) return;
            this[i] = '\u00A0' + this[i];
        });

        if( negative ) return '-' + value.slice(0, index) + a.join('')
        else return value.slice(0, index) + a.join('');
    }
    function getSplitter(field) {
        var splitter = /\./;
        if( field.value.indexOf(',') != -1 ) splitter = /,/;
        return splitter;
    }
    function forEach(list, callback) {
        if( list )
            for(var i = 0; i < list.length; i++)
                callback.call(list, i);
    }

    CBS.deleteMask = function(field) {
        if( field.isContent )
            var properties = ['isContent', 'maskName', 'isAmount', 'removeAllEventListeners'];
        else
            var properties = ['mask', 'maxLength', 'template', 'startPrint', 'removeAllEventListeners'];

        field.removeAllEventListeners();
        field.value = '';
        for(var i = 0; i < properties.length; i++)
            delete field[properties[i]];
    };

    CBS.isValid = function(field) {
        if( field.template && field.value )
            return (field.value.indexOf(space) == -1);
        else
            return !!field.value;
    };

    function bindEvent(elem, typeEvent, handler) {
        if( elem.addEventListener )
            elem.addEventListener(typeEvent, handler, false);
        else if( elem.attachEvent )
            elem.attachEvent("on" + typeEvent, handler);
    }
    function unbindEvent(elem, typeEvent, handler) {
        if( elem.removeEventListener )
            elem.removeEventListener(typeEvent, handler, false);
        else if( elem.detachEvent )
            elem.detachEvent("on" + typeEvent, handler);
    }

    // Polyfill for versions of IE before IE_9
    // Production steps of ECMA-262, Edition 5, 15.4.4.14
    // Reference: http://es5.github.io/#x15.4.4.14
    if( !Array.prototype.indexOf ) {
        Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;

            if ( this == null ) throw new TypeError('"this" is null or not defined');

            var o = Object(this);
            var len = o.length >>> 0;
            if (len === 0) return -1;

            var n = +fromIndex || 0;
            if (Math.abs(n) === Infinity) n = 0;
            if (n >= len) return -1;

            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
            while (k < len) {
                if (k in o && o[k] === searchElement)
                    return k;
                k++;
            }
            return -1;
        };
    }
})();