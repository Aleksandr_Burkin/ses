(function() {
    window.log = function() {
        console.log.apply(console, arguments);
    }
    window.warn = function() {
        console.warn.apply(console, arguments);
    }
    window.error = function() {
        console.error.apply(console, arguments);
    }

    window.performSecureRequest = function(url, methodName, successFunction, errorFunction) {
        let token = localStorage.getItem('SeS_token');

        var allHeaders = new Headers();
        allHeaders.append('X-Authorization', 'Bearer ' + JSON.parse(token).token);
        allHeaders.append('Cache-Control', 'no-cache');

        var reqInit = {
            method: methodName,
            headers: allHeaders
            //credentials: 'include'
        };

        fetch('http://localhost:9090/api' + url, reqInit)
            .then(function(response) {
                if (response.status !== 200) {
                    let type = typeof errorFunction;
                    if (type != 'undefined' )
                        errorFunction();
                    else
                        error('error executing secure request!');
                    return;
                }
                response.text().then(function(text) {
                    var data = text ? JSON.parse(text) : {};
                    successFunction(data);
                });
            });
    }

    window.formatMoney = function(value) {
        return (value + '').split('').reverse().join('')
               .replace(/(\d{3})/g, "$1 ").split('')
               .reverse().join('').replace(/^ /, '');
    }
})();