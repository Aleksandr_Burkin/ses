import { browserHistory } from 'react-router'

import { GET_BUSINESSMAN_INFO,
         GET_TAPE_BUSINESSMAN_MESSAGES,
         GET_PERSONAL_COMPANIES,
         GET_FREE_COMPANIES,
         BUY_COMPANY,
         GET_COMPANY,
         GET_TAPE_COMPANY_MESSAGES,
         INVEST,
         GET_PARTNERS,
         GET_PRODUCTS,
         BUY_PRODUCT,
         GET_ASSETS,
         INTO_PRODUCTS,
         GET_PERSONAL_PRODUCTS,
         CHANGE_MARKET_COST,
         ADD_TAPE_MESSAGE } from '../constants/ActionConst'

export function getBusinessmanInfo() {
    return (dispatch) => {
        let url = '/businessmanInfo';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_BUSINESSMAN_INFO,
                payload: data
            })
        }).bind(this));
    }
}

export function getTapeBusinessmanMessages() {
    return (dispatch) => {
        let url = '/tapeBusinessmanMessages';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_TAPE_BUSINESSMAN_MESSAGES,
                payload: data
            })
        }).bind(this));
    }
}

export function getPersonalCompanies() {
    return (dispatch) => {
        let url = '/personalCompanies';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_PERSONAL_COMPANIES,
                payload: data
            })
        }).bind(this));
    }
}

export function getFreeCompanies() {
    return (dispatch) => {
        let url = '/freeCompanies';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_FREE_COMPANIES,
                payload: data
            })
        }).bind(this));
    }
}

export function buyCompany(inn) {
    return (dispatch) => {
        let url = '/buyCompany/' + inn;
        performSecureRequest(url, 'PUT', (function(data) {
            dispatch({
                type: BUY_COMPANY,
                payload: data
            });
            log('success buy company');
            browserHistory.push('/');
        }).bind(this), function() {
            error('error buy company');
        });
    };
}

export function getCompany(inn) {
    return (dispatch) => {
        let url = '/company/' + inn;
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_COMPANY,
                payload: data
            })
        }).bind(this));
    }
}

export function getTapeCompanyMessages(inn) {
    return (dispatch) => {
        let url = '/tapeCompanyMessages/' + inn;
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_TAPE_COMPANY_MESSAGES,
                payload: data
            })
        }).bind(this));
    }
}

export function invest(investment) {
    return (dispatch) => {
        var url = '/invest/' + investment;
        performSecureRequest(url, 'PUT', (function(data) {
            dispatch({
                type: INVEST,
                payload: data
            })
        }).bind(this));
    }
}

export function getPartners() {
    return (dispatch) => {
        var url = '/compatibleCompanies';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_PARTNERS,
                payload: data
            })
        }).bind(this));
    }
}

export function getProducts(inn) {
    return (dispatch) => {
        var url = '/products/' + inn;
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_PRODUCTS,
                payload: data,
                inn: inn
            })
        }).bind(this));
    }
}

export function buyProduct(toCompanyINN, productId, count) {
    return (dispatch) => {
        var url = '/buyProduct/' + toCompanyINN + '/' + productId + '/' + count;
        performSecureRequest(url, 'POST', (function(data) {
            dispatch({
                type: BUY_PRODUCT,
                payload: data
            })
        }).bind(this));
    }
}

export function getAssets() {
    return (dispatch) => {
        var url = '/assets';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_ASSETS,
                payload: data
            })
        }).bind(this));
    }
}

export function intoProducts(id, count) {
    return (dispatch) => {
        var url = '/intoProducts/' + id + '/' + count;
        performSecureRequest(url, 'PUT', (function(data) {
            dispatch({
                type: INTO_PRODUCTS,
                payload: data,
                id: id
            })
        }).bind(this));
    }
}

export function getPersonalProducts() {
    return (dispatch) => {
        var url = '/personalProducts';
        performSecureRequest(url, 'GET', (function(data) {
            dispatch({
                type: GET_PERSONAL_PRODUCTS,
                payload: data
            })
        }).bind(this));
    }
}

export function changeMarketCost(id, marketCost) {
    return (dispatch) => {
        var url = '/changeMarketCost/' + id + '/' + marketCost;
        performSecureRequest(url, 'PUT', (function() {
            dispatch({
                type: CHANGE_MARKET_COST,
                payload: marketCost,
                id: id
            })
        }).bind(this));
    }
}

export function addTapeMessage(message) {
    return (dispatch) => {
        dispatch({
            type: ADD_TAPE_MESSAGE,
            payload: message
        })
    }
}