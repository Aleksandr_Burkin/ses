import { combineReducers } from 'redux'
import businessman from './businessman'
import companies from './companies'
import company from './company'
import partners from './partners'
import tape from './tape'

export default combineReducers({
    businessman,
    companies,
    company,
    partners,
    tape
})