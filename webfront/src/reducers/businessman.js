import update from 'react-addons-update'

import { GET_BUSINESSMAN_INFO,
         GET_PERSONAL_COMPANIES,
         BUY_COMPANY,
         INVEST } from '../constants/ActionConst'

const initialState = {
    info: {},
    companies: []
};

export default function businessman(state = initialState, action) {
    switch (action.type) {
        case GET_BUSINESSMAN_INFO:
            return	{ ...state, info: action.payload };
        case GET_PERSONAL_COMPANIES:
            return	{ ...state, companies: action.payload };
        case BUY_COMPANY:
        case INVEST:
            let cashResponse = update(state.info, {cash: {$set: action.payload.cash}});
            return	{ ...state, info: cashResponse };
        default:
            return state
    }
}
