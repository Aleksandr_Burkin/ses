import { GET_FREE_COMPANIES } from '../constants/ActionConst'

var stub = [];
stub.push({
    inn: 0,
    name: '',
    address: '',
    logo: '',
    price: '',
    economicType: ''
});
const initialState = stub;

export default function companies(state = initialState, action) {
    switch (action.type) {
        case GET_FREE_COMPANIES:
            return action.payload;
        default:
            return state
    }
}