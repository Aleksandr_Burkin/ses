import { GET_TAPE_BUSINESSMAN_MESSAGES,
         GET_TAPE_COMPANY_MESSAGES,
         ADD_TAPE_MESSAGE } from '../constants/ActionConst'

const initialState = {
    title: '',
    icon: '',
    messages: []
};

export default function tape(state = initialState, action) {
    switch (action.type) {
        case GET_TAPE_BUSINESSMAN_MESSAGES:
            return	action.payload;
        case GET_TAPE_COMPANY_MESSAGES:
            return	action.payload;
        case ADD_TAPE_MESSAGE:
            let list = state.messages;
            list.unshift(action.payload);
            return	{ ...state, messages: list };
        default:
            return state
    }
}