import update from 'react-addons-update'

import { GET_COMPANY,
         INVEST,
         BUY_PRODUCT,
         GET_ASSETS,
         INTO_PRODUCTS,
         GET_PERSONAL_PRODUCTS,
         CHANGE_MARKET_COST } from '../constants/ActionConst'

const initialState = {
        info: {
            inn: 0,
            name: '',
            address: '',
            logo: '',
            background: '',
            slogan: '',
            budget: {
                value: ''
            },
            economicType: ''
        },
        assets: {},
        products: {}
};

export default function company(state = initialState, action) {
    switch (action.type) {
        case GET_COMPANY:
            return	{ ...state, info: action.payload };
        case INVEST:
        case BUY_PRODUCT:
            return update(state, {info: {budget: {$set: action.payload.budget}}});
        case GET_ASSETS:
            let assets = {};
            action.payload.map(function(item) {
                assets[item.id] = item;
            });

            return	{ ...state, assets: assets };
        case INTO_PRODUCTS:
            let count = action.payload.count;
            if( count ) {
                return update(state, {assets: {[action.id]: {count: {$set: action.payload.count}}}});
            } else {
                delete state.assets[action.id];
                return { ...state };
            }
        case GET_PERSONAL_PRODUCTS:
            let products = {};
            action.payload.map(function(item) {
                products[item.id] = item;
            });
            return	{ ...state, products: products };
        case CHANGE_MARKET_COST:
            return update(state, {products: {[action.id]: {marketCost: {$set: action.payload}}}});
        default:
            return state;
    }
}