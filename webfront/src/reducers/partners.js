import update from 'react-addons-update'

import { GET_PARTNERS,
         GET_PRODUCTS,
         BUY_PRODUCT } from '../constants/ActionConst'

var template = [
    {
        info: {
            inn: 0,
            name: '',
            address: '',
            logo: '',
            background: '',
            slogan: '',
            economicType: '',
        },
        products: {
            id: 0,
            name: '',
            description: '',
            logo: '',
            marketPrice: 0
        }
    }
];

const initialState = [];

export default function partners(state = initialState, action) {
    switch (action.type) {
        case GET_PARTNERS:
            let partners = {};
            action.payload.map(function(item) {
                let products = (state[item.inn]) ? state[item.inn].products : [];
                partners[item.inn] = {info: item, products: products}
            });
            return partners;
        case GET_PRODUCTS:
            let productsResponse = update(state[action.inn], {products: {$set: action.payload}});
            return	{ ...state, [action.inn]: productsResponse };
        case BUY_PRODUCT:
            return state;
        default:
            return state
    }
}