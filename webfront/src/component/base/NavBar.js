import React, {Component} from 'react'
import { Link } from 'react-router'

export default class NavBar extends Component {

	shouldComponentUpdate() {
		return false;
	}

    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                	    <Link to='/'>
                	        <img src="/resources/image/logo.png" className="img-responsive"/>
                	    </Link>
                    </div>
                    <div id="nav-menu">
                	    <ul className="nav navbar-nav">
                	        <li>
                	            <Link to='/'>
                	                <span className="glyphicon glyphicon-home" aria-hidden="true"/>
                	                Главная
                	            </Link>
                	        </li>
                	        <li>
                	          	<Link to='/searchCompanies'>
                                    <span className="glyphicon glyphicon-copyright-mark" aria-hidden="true"/>
                                    Компании
                                </Link>
                	        </li>
                	        <li>
                	            <Link to='#'>
                	                <span className="glyphicon glyphicon-star" aria-hidden="true"/>
                	            	Бренды
                	          	</Link>
                	        </li>
                	        <li>
                	            <Link to='#'>
                	                <span className="glyphicon glyphicon-globe" aria-hidden="true"/>
                	            	Карта
                	          	</Link>
                	        </li>
							<li>
								<Link to='#'>
									<span className="glyphicon glyphicon-fire" aria-hidden="true"/>
									Аукцион
								</Link>
							</li>
                	        <li>
                	            <Link to='#'>
                	                <span className="glyphicon glyphicon-cog" aria-hidden="true"/>
                	            	Настройки
                	          	</Link>
                	        </li>
                	    </ul>
                	    <a id="log-out" href="#" className="pull-right">
                	        <span className="glyphicon glyphicon-log-out" aria-hidden="true"/>
                	    </a>
                    </div>
                </div>
            </nav>
        )
    }
}