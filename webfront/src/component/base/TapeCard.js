import React, {Component, renderToString} from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router'

import $ from 'jquery'

export default class TapeCard extends Component {

    componentDidMount() {
        this.connectToWebsocket();
    }

    connectToWebsocket() {
        var socket = new SockJS('http://localhost:9090/tape-websocket');
        var stompClient = Stomp.over(socket);
        stompClient.connect({}, (function (frame) {
            //log('Connected: ' + frame);
            stompClient.subscribe('/topic/tape', (function (response) {
                var data = response ? JSON.parse(response.body) : {};
                this.addMessage(data);
            }).bind(this));
        }).bind(this));
    }

    addMessage(message) {
        this.props.actions.addTapeMessage(message);
        var $parent = $("#tape-card .panel-body");
        $parent.find('div:first-child').css('opacity', '0');
        $parent.find('div:first-child').fadeTo('slow', 1);
    }

    render() {
        //TODO заменить на другие key
        var nextKey = 1;
        var messages = (this.props.tape.messages.length != 0)
                       ?
                        this.props.tape.messages.map(message =>
                            <Message key={nextKey++} message={message}/>
                        )
                       :
                        <strong>Нет событий</strong>;
        let icon = (this.props.tape.icon)
                   ?
                    <img className='icon' src={this.props.tape.icon}/>
                   :
                    '';
        return (
            <div id="tape-card" className="panel panel-default">
                <div className="panel-heading">
                    <div className="panel-title">
                        {icon}
                        <span> {this.props.tape.title} </span>
                        <a className='search'>
                            <span className="glyphicon glyphicon-search" aria-hidden="true"/>
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    {messages}
                </div>
            </div>
        )
    }
}

class Message extends Component {

    render() {
        let markup = {__html: this.props.message.text};
        let date = {__html: this.props.message.date};
        return (
            <div className="tape-message">
                <span className="time" dangerouslySetInnerHTML={date}/>
                <span dangerouslySetInnerHTML={markup}/>
            </div>
        )
    }
}