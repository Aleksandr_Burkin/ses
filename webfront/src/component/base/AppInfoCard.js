import React, {Component} from 'react';

export default class AppInfoCard extends Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <div id="app-info-card" className="panel panel-default">
                <div className="panel-heading">
                    <div className="panel-title">
                        Copyright © Sescoin, 2017
                    </div>
                </div>
                <div className="panel-body">
                    <ul className="list-unstyled list-inline">
                        <li><a href="#">О стратегии</a></li>
                        <li><a href="#">Помощь</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}