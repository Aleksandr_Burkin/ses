import React, {Component} from 'react'
import { Link } from 'react-router'

export default class BusinessmanCard extends Component {

    render() {
        const {firstName, lastName, avatar} = this.props.businessman

        return (
            <div id="businessman-card" className="panel panel-default">
                <div className="panel-heading">
                    <div className="panel-title">
                	    <span> {firstName} {lastName} </span>
                	</div>
                </div>
                <div className="panel-body">
                    <img src={avatar} className="avatar img-thumbnail img-responsive"/>
                    <ul className="stats list-unstyled">
                        <li className="rank">
                            {/*<span className="glyphicon glyphicon-star glyphicon-color" aria-hidden="true"/>*/}
                                <strong>Ранг: </strong> 1
                        </li>
                        <li className="rating">
                            {/*<span className="glyphicon glyphicon-flash glyphicon-color" aria-hidden="true"/>*/}
                            <strong>Рейтинг: </strong> 1000
                        </li>
                        <li className="rating">
                            {/*<span className="glyphicon glyphicon-flash glyphicon-color" aria-hidden="true"/>*/}
                            <strong>Лояльность: </strong> 8/10
                        </li>
                    </ul>
                    <hr/>
                    <ul className="menu list-unstyled">
                        <li>
                            <Link to='/'>
                                <span className="glyphicon glyphicon-copyright-mark" aria-hidden="true"/>
                                Предприятия
                            </Link>
                        </li>
                        <li>
                            <Link to='/'>
                                <span className="glyphicon glyphicon-stats" aria-hidden="true"/>
                                Статистика
                            </Link>
                        </li>
                        <li>
                            <Link to='/'>
                                <span className="glyphicon glyphicon-user" aria-hidden="true"/>
                                Партнеры
                            </Link>
                        </li>
                        <li>
                            <Link to='/'>
                                <span className="glyphicon glyphicon-glass" aria-hidden="true"/>
                                Беседы
                            </Link>
                        </li>
                    </ul>
                </div>
           </div>
        )
    }
}