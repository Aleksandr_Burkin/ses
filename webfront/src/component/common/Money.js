import React, {Component} from 'react';

export default class Money extends Component {

    render() {
        var className = "money" + (this.props.className ? ' ' + this.props.className : '');
        return (
            <span className={className}>{formatMoney(this.props.value)} &#8381; </span>
        )
    }
}