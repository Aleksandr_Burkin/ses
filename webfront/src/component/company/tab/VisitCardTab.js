import React, {Component} from 'react';

export default class VisitCardTab extends Component {

    render() {
        let company = this.props.company;
        return (
            <div id="visit-card" role="tabpanel" className="tab-pane active" >
                <div className="visit-card">
                    <img src={company.background} className="img-responsive"/>
                    <div className="description">
                        <div className="row">
                            <div className="col-md-2">
                                <div className="logo-wrapper img-thumbnail">
                                    <img src={company.logo} className="company-logo img-responsive"/>
                                </div>
                            </div>
                            <div className="col-md-10">
                                <span className="company-name h4"> {company.name} </span>
                                <br/>
                                <em className="activity"> {company.economicType} </em>
                            </div>
                        </div>
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td><strong>Владелец:</strong></td>
                                    <td>Александр Буркин</td>
                                </tr>
                                <tr>
                                    <td><strong>Адрес:</strong></td>
                                    <td>{company.address}</td>
                                </tr>
                                <tr>
                                    <td><strong>Слоган:</strong></td>
                                    <td className="slogan">{company.slogan}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" className="on-map btn btn-default" aria-label="Left Align">
                        <span className="glyphicon glyphicon-globe" aria-hidden="true"/>
                    </button>
                </div>
            </div>
        )
    }
}