import React, {Component} from 'react'
import $ from 'jquery'
import ReactDOM from 'react-dom'

window.jQuery = require('jquery')
require('bootstrap-select')
require('bootstrap-select/sass/bootstrap-select.scss')
require('bootstrap-select/sass/variables.scss')

import Money from '../../common/Money'

export default class PartnersTab extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let partners = this.props.partners;
        let inns = Object.keys(partners);
        return (
            (inns.length != 0)
            ?
                <div  id="partners" role="tabpanel" className="tab-pane">
                    {
                        inns.map(inn =>
                            <PartnerVisitCard key={inn} company={partners[inn]} actions={this.props.actions}/>
                        )
                    }
                </div>
            :
                <div  id="partners" role="tabpanel" className="tab-pane">
                    <strong> Загрузка... </strong>
                </div>
        )
    }
}

class PartnerVisitCard extends Component {

    buyProduct() {
        this.props.actions.getProducts(this.props.company.info.inn);
        $(ReactDOM.findDOMNode(this)).find('.panel-deal').css('display', 'block');
    }

    render() {
        let companyInfo = this.props.company.info;
        return (
            <div className="visit-card">
                <img src={companyInfo.background} className="img-responsive"/>
                <div className="description">
                    <div className="row">
                        <div className="col-md-2">
                            <div className="logo-wrapper img-thumbnail">
                                <img src={companyInfo.logo} className="company-logo img-responsive"/>
                            </div>
                        </div>
                        <div className="col-md-10">
                            <span className="company-name h4"> {companyInfo.name} </span>
                            <br/>
                            <em className="activity"> {companyInfo.economicType} </em>
                        </div>
                    </div>
                    <table className="table">
                        <tbody>
                            <tr>
                                <td><strong>Владелец:</strong></td>
                                <td>Государство</td>
                            </tr>
                            <tr>
                                <td><strong>Адрес:</strong></td>
                                <td> {companyInfo.address} </td>
                            </tr>
                            <tr>
                                <td><strong>Слоган:</strong></td>
                                <td className="slogan"> {companyInfo.slogan} </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="button" className="add-deal btn btn-default" aria-label="Left Align"
                            onClick={::this.buyProduct}>
                        <span className="glyphicon glyphicon-plus-sign" aria-hidden="true"/>
                    </button>
                    <Products inn={companyInfo.inn} products={this.props.company.products} actions={this.props.actions}/>
                </div>
                <button type="button" className="on-map btn btn-default" aria-label="Left Align">
                    <span className="glyphicon glyphicon-globe" aria-hidden="true"/>
                </button>
            </div>
        )
    }
}

class Products extends Component {

    constructor(props) {
        super(props);
    }

    //TODO подумать куда перенести (вызывается несколько раз)
    componentDidUpdate() {
        $('.selectpicker').selectpicker();
    }

    okDeal() {
        let $parent = $(ReactDOM.findDOMNode(this));
        let inn = this.props.inn;
        let productId = $parent.find('option:selected').data('id');
        let count = $parent.find('.product-count').val();
        this.props.actions.buyProduct(inn, productId, count);
        $parent.css('display', 'none');
    }

    cancelDeal() {
        $(ReactDOM.findDOMNode(this)).css('display', 'none');
    }

    onChangeProduct(e) {
        //TODO подумать... Быть может использовать redux или некую функцию из Money.class
        let $parent = $(e.target).parent().parent();
        let price = parseInt($parent.find('.text-muted').html());
        let count = $parent.find('.product-count').val();
        let total = formatMoney(price * count) + ' &#8381;';
        $parent.find('.total').html(total);

        //TODO подумать и здесь...
        let optionNumber = $(e.target).find('option:selected').data('number');
        let logo = this.props.products[optionNumber].logo;
        let description = this.props.products[optionNumber].description;

        $parent.parent().find('.product-logo').attr('src', logo);
        $parent.parent().find('.product-description').html(description);
    }

    onChangeProductCount(e) {
        //TODO подумать... Быть может использовать redux или некую функцию из Money.class
        let $parent = $(e.target).parent();
        let price = parseInt($parent.find('.text-muted').html());
        let count = $(e.target).val();
        let total = formatMoney(price * count) + ' &#8381;';
        $parent.find('.total').html(total);
    }

    render() {
        //TODO подумать насчет data-id (безопасность)
        var counter = 0;
        let products = this.props.products.map(product =>
                        <option key={product.id}
                                data-id={product.id}
                                data-number={counter++}
                                data-subtext={product.marketCost + ' &#8381;'}>
                            {product.name}
                        </option>
                    );
        return (
            (this.props.products.length != 0)
            ?
                <div className="panel-deal">
                    <div className="calculator">
                        <select data-show-subtext="true" className="selectpicker show-tick"
                                onChange={::this.onChangeProduct}>
                            {products}
                        </select>
                        <span className="glyphicon glyphicon-remove" aria-hidden="true"/>
                        <input type="text" className="form-control product-count"
                               onChange={this.onChangeProductCount}/>
                        <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"/>
                        <Money className='total' value={0}/>
                    </div>
                    <hr/>
                    <div className="product">
                        <div>
                            <img src={this.props.products[0].logo} className="product-logo img-responsive"/>
                        </div>
                        <div>
                            <em className="product-description">
                                {this.props.products[0].description}
                            </em>
                        </div>
                    </div>
                    <button type="button" className="ok btn btn-default" aria-label="Left Align"
                            onClick={::this.okDeal}>
                        <span className="glyphicon glyphicon-ok-sign" aria-hidden="true"/>
                    </button>
                    <button type="button" className="cancel btn btn-default" aria-label="Left Align"
                            onClick={::this.cancelDeal}>
                        <span className="glyphicon glyphicon-remove-sign" aria-hidden="true"/>
                    </button>
                </div>
            :
                <div className="panel-deal">Нет продуктов</div>
        )
    }
}