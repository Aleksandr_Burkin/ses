import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import $ from 'jquery'

export default class BusinessTab extends Component {

    constructor(props) {
        super(props);
    }

    /*shouldComponentUpdate(nextProps) {
        let newKeys = Object.keys(nextProps.products);
        let oldKeys = Object.keys(this.props.products);
        if( newKeys.length != oldKeys.length ) return true;

        for(var i = 0; i < newKeys.length; i++ ) {
            let newKey = newKeys[i];
            if( !(newKey in this.props.products ) )
                return true;

        }

        return false;
    }*/

    render() {
        let ids = Object.keys(this.props.products);
        let view = (
                ( ids.length )
                ?
                    <div>
                        <Products products={this.props.products} actions={this.props.actions}/>
                        <FinancialResult/>
                    </div>
                :
                    <strong>У компании нет продуктов</strong>
        );
        return (
            <div id="business" role="tabpanel" className="tab-pane">
                {view}
            </div>
        )
    }
}

class Products extends Component {

    constructor(props) {
        super(props);
        this.currentProductId = 0;
    }

    onChangeProduct(e) {
        let $parent = $(e.target).parent();
        $parent.parent().find('.product-name').css('text-decoration', 'none').css('color', '#1485cc');
        $(e.target).css('text-decoration', 'underline').css('color', '#23527c');
        let productId = $parent.data('id');
        this.currentProductId = productId;
        let primeCost = this.props.products[productId].primeCost;
        let marketCost = this.props.products[productId].marketCost;
        let count = this.props.products[productId].count;

        let $personalProduct = $parent.closest('.personal-product');
        $personalProduct.find('.count').html(count);
        $personalProduct.find('.prime-cost').html(primeCost + ' &#8381;');
        //$personalProduct.find('.market-cost').attr('placeholder', marketCost);
        $personalProduct.find('.market-cost').val(marketCost);
    }

    setMarketCost() {
        let newMarketCost = ReactDOM.findDOMNode(this.refs.marketCostField).value;
        this.props.actions.changeMarketCost(this.currentProductId, newMarketCost);
    }

    render() {
        let products = this.props.products;
        let ids = Object.keys(products);
        var firstId = ids[0];
        this.currentProductId = firstId;
        return (
            <div className='personal-product'>
                <div className='cell'>
                    <ul className="list-unstyled">
                        {
                            ids.map(id =>
                                <li key={products[id].id} data-id={products[id].id}>
                                    <img src={products[id].logo} className="icon img-responsive"/>
                                    <a className='product-name'
                                       onClick={::this.onChangeProduct}>{products[id].name}</a>
                                </li>
                            )
                        }
                    </ul>
                </div>
                <div className='cell'>
                    <table>
                        <tbody>
                        <tr>
                            <td>Количество:</td>
                            <td>
                                <div className='count'>{products[firstId].count} </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Себестоимость:</td>
                            <td>
                                <div className='prime-cost'>{products[firstId].primeCost} &#8381;</div>
                            </td>
                        </tr>
                        <tr>
                            <td>Рыночная цена:</td>
                            <td>
                                <div className="input-group">
                                    <input className='form-control market-cost'
                                           ref="marketCostField"
                                           type="text" defaultValue={products[firstId].marketCost} />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default"
                                                type="button"
                                                onClick={::this.setMarketCost}>
                                            <span className="glyphicon glyphicon-ok" aria-hidden="true"/>
                                        </button>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

class FinancialResult extends Component {

    componentDidMount() {
        this.connectToWebsocket();
    }

    connectToWebsocket() {
        var socket = new SockJS('http://localhost:9090/financial-websocket');
        var stompClient = Stomp.over(socket);
        stompClient.connect({}, (function (frame) {
            log('Connected: ' + frame);
            stompClient.subscribe('/topic/financialResult', (function (response) {
                var data = response ? JSON.parse(response.body) : {};
                this.refreshFinancialResult(data);
            }).bind(this));
        }).bind(this));
    }

    refreshFinancialResult(data) {
        let $overhead = $('.overhead');
        let $netProfit = $('.net-profit');
        $overhead.css('opacity', '0');
        $overhead.html(data.overhead + ' &#8381;');
        $overhead.fadeTo('slow', 1);
        $netProfit.css('opacity', '0');
        $netProfit.html(data.netProfit + ' &#8381;');
        $netProfit.fadeTo('slow', 1);
    }

    render() {
        return (
            <div className='financial-result'>
                <strong>Финансовые результаты</strong>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>Накладные расходы</td>
                            <td className="text-center"><span className="overhead money"> 0 &#8381; </span></td>
                        </tr>
                        <tr>
                            <td>Чистая прибыль</td>
                            <td className="text-center"><span className="net-profit money"> 0 &#8381; </span></td>
                        </tr>
                        <tr>
                            <td>Рентабельность</td>
                            <td className="text-center"><span className="profitability"> 0 % </span></td>
                        </tr>
                        <tr className="footer">
                            <td colSpan="2">
                                <div className="timer pull-right">
                                    <span className="glyphicon glyphicon-hourglass glyphicon-color" aria-hidden="true"/>
                                    <span>05:43</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}