import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import $ from 'jquery'

export default class AssetsTab extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        //TODO добавить "загрузку"
        let ids = Object.keys(this.props.assets);
        let assets = (
                (ids.length != 0)
                ?
                    ids.map(id =>
                        <Assets key={id} asset={this.props.assets[id]} actions={this.props.actions}/>
                    )
                :
                    <strong>У компании нет активов</strong>
        );

        return (
            <div id='assets' role='tabpanel' className='tab-pane'>
                {assets}
            </div>
        )
    }
}


class Assets extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    intoProducts(e) {
        let id = $(e.target).closest('.asset').data('asset-id');
        let count = ReactDOM.findDOMNode(this.refs.countIntoProducts).value;
        this.props.actions.intoProducts(id, count);
    }

    render() {
        let asset = this.props.asset;
        return (
            <div className='asset' data-asset-id={asset.id}>
                <div className='row'>
                    <div className='col-md-9'>
                        <div className='name'>{asset.name}</div>
                        <div className='info'>
                            <div>
                                <img src={asset.logo} className='asset-logo img-responsive'/>
                            </div>
                            <div>
                                <em className='description'> {asset.description} </em>
                                <div className='copyright'>Copyright © <a href='#'> {asset.owner} </a></div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3'>
                        <div className='count'> {asset.count} </div>
                        <div className="input-group">
                            <input ref="countIntoProducts" type="text" className="form-control"/>
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button" onClick={::this.intoProducts}>
                                    <span className="glyphicon glyphicon-ok" aria-hidden="true"/>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

