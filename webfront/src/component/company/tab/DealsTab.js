import React, {Component} from 'react';

export default class DealsTab extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div id="deals" role="tabpanel" className="tab-pane">
                <div className="row">
                    <div className="col-md-12">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th colSpan="2">Компания</th>
                                    <th>Благо</th>
                                    <th>Количество</th>
                                    <th>Цена (1шт.)</th>
                                    <th>Итого</th>
                                    <th></th>
                                </tr>
                            </thead>
                             <tbody>
                                <tr>
                                    <td className="text-center">
                                        <img src="/resources/image/company/bulka.png" className="company-logo img-responsive"/>
                                    </td>
                                    <td className="text-center">
                                        Булка
                                    </td>
                                    <td className="text-center">
                                        Булки
                                    </td>
                                    <td className="text-center">
                                        1 000
                                    </td>
                                    <td className="text-center money">
                                        10 &#8381;
                                    </td>
                                    <td className="text-center money">
                                        10 000 &#8381;
                                    </td>
                                    <td className="text-center">
                                        <button type="button" className="btn btn-default" aria-label="Left Align">
                                            <span className="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}