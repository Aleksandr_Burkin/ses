import React, {Component} from 'react'
import { Link } from 'react-router'

export default class PersonalCompanies extends Component {

    componentDidMount() {
        this.props.actions.getPersonalCompanies();
        this.props.actions.getTapeBusinessmanMessages();
    }

    render() {
        var companies = this.props.companies.map(company =>
            <CompanyWrapper key={company.inn} company={company}/>
        );
        return (
             this.props.companies.length == 0
             ?
                 <div>
                     На данный момент Вы не владеете предприятиями. <br/>
                     Вы можете <Link to='searchCompanies'>приобрести</Link> компанию и
                     начать строить Ваш собственный бизнес.
                 </div>
             :
                 <div id="my-companies" className="panel panel-default">
                     <div className="panel-body">
                         {companies}
                     </div>
                 </div>
        )
    }
}

class CompanyWrapper extends Component {

    render() {
        return (
            <Link to={'/company/' + this.props.company.inn}>
                <div className="row">
                    <div className="col-md-2">
                        <img src={this.props.company.logo} className="company-logo img-responsive"/>
                    </div>
                    <div className="col-md-10 reset-padding-left">
                        <div className="company-info">
                            <span className="company-name h4"> {this.props.company.name} </span>
                            <br/>
                            <div className="deals-count">
                                <span className="glyphicon glyphicon-ok glyphicon-color" aria-hidden="true"/>
                                <strong>Сделки:</strong> <span className="count">17</span>
                                <span className="plus">(+2)</span>
                            </div>
                            <div className="partners-count">
                                <span className="glyphicon glyphicon-file glyphicon-color" aria-hidden="true"/>
                                <strong>Агенты:</strong> <span className="count">7</span>
                            </div>
                        </div>
                        <div className="profit-up pull-right">
                            <span className="glyphicon glyphicon-arrow-up" aria-hidden="true"/>
                            <strong> 100 000 &#8381; </strong>
                        </div>
                    </div>
                </div>
            </Link>
        )
    }
}