import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actions from '../../actions/Actions'

import Money from '../common/Money'
import VisitCardTab from './tab/VisitCardTab'
import BusinessTab from './tab/BusinessTab'
import PartnersTab from './tab/PartnersTab'
import AssetsTab from './tab/AssetsTab'
import DealsTab from './tab/DealsTab'

export default class Company extends Component {

    componentDidMount() {
        this.props.actions.getCompany(this.props.params.inn);
        this.props.actions.getTapeCompanyMessages(this.props.params.inn);
    }

    changeTab(e) {
        switch (e.target.innerHTML) {
            case 'Агенты' :
                this.props.actions.getPartners();
                break;
            case 'Активы' :
                this.props.actions.getAssets();
                break;
            case 'Бизнес' :
                this.props.actions.getPersonalProducts();
        }
    }

    invest() {
        let field = ReactDOM.findDOMNode(this.refs.investmentField);
        let investment = field.value.replace(/\s/g, '');
        field.value = '';
        this.props.actions.invest(investment);
    }

    //TODO заменить на маску
    onChangeInvestmentField(e) {
        let val = e.target.value.replace(/\s/g, '');
        e.target.value = formatMoney(val);
    }

    render() {
        let company = this.props.company;
        return (
            <div id="company" className="panel panel-default">
                <div className="panel-body">
                    <div className="investment">
                        <div>
                            <input ref="investmentField"
                                   type="text"
                                   className="form-control"
                                   placeholder='инвестиции:'
                                   onChange={this.onChangeInvestmentField}/>
                            <button className="btn btn-default to-invest" type="button" onClick={::this.invest}>
                                <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                            </button>
                        </div>
                        <Money className='budget pull-right' value={company.info.budget}/>
                    </div>
                    <ul className="nav nav-tabs" role="tablist">
                        <li role="presentation" className="active">
                            <a href="#visit-card" aria-controls="visit-card" role="tab" data-toggle="tab"
                               onClick={::this.changeTab}>
                                Визитка
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#partners" aria-controls="partners" role="tab" data-toggle="tab"
                               onClick={::this.changeTab}>
                                Агенты
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#assets" aria-controls="assets" role="tab" data-toggle="tab"
                               onClick={::this.changeTab}>
                                Активы
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#business" aria-controls="business" role="tab" data-toggle="tab"
                               onClick={::this.changeTab}>
                                Бизнес
                            </a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <VisitCardTab company={company.info}/>
                        <PartnersTab partners={this.props.partners} actions={this.props.actions}/>
                        <AssetsTab assets={company.assets} actions={this.props.actions}/>
                        <BusinessTab products={company.products} actions={this.props.actions}/>
                    </div>
                </div>
            </div>
        )
    }
}