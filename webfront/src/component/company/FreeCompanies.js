import React, {Component} from 'react'
import Money from '../common/Money'
import { browserHistory } from 'react-router'

export default class FreeCompanies extends Component {

    componentDidMount() {
        this.props.actions.getFreeCompanies();
    }

    render() {
        return (
            (this.props.companies[0].inn != 0)
             ?
             <div id="search-company" className="panel panel-default">
                <div className="panel-body">
                    {
                        this.props.companies.map(company =>
                            <CompanyWrapper key={company.inn} company={company} actions={this.props.actions}/>
                        )
                    }
                </div>
             </div>
             :
             <strong> Загрузка... </strong>
        )
    }
}

class CompanyWrapper extends Component {

    buyCompany() {
        this.props.actions.buyCompany(this.props.company.inn);
    }

    render() {
        let company = this.props.company;
        return (
            <div className="row">
                <div className="col-md-2">
                    <img src={company.logo} className="company-logo img-responsive"/>
                </div>
                <div className="col-md-7 reset-padding-left">
                    <span className="company-name h4"> {company.name} </span>
                    <br/>
                    <em> {company.economicType} </em>
                    <br/>
                    <strong className="address"> {company.address} </strong>
                </div>
                <div className="col-md-3">
                    <Money value={company.price} className="company-price pull-right"/>
                    <button className="btn btn-default pull-right" type="button" onClick={::this.buyCompany}>
                        Приобрести
                    </button>
                </div>
            </div>
        )
    }
}