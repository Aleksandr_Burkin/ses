import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actions from '../actions/Actions'

import NavBar from '../component/base/NavBar'
import BusinessmanCard from '../component/base/BusinessmanCard'
import TapeCard from '../component/base/TapeCard'
import AppInfoCard from '../component/base/AppInfoCard'
import Money from '../component/common/Money'
import PersonalCompanies from '../component/company/PersonalCompanies'

@connect(mapStateToProps, mapDispatchToProps)
export default class App extends Component {

    componentDidMount() {
        this.props.actions.getBusinessmanInfo();
    }

    render() {
        let actions = this.props.actions;
        const businessman = this.props.businessman;
        let child;
        switch (this.props.children.type.name) {
            case 'PersonalCompanies' :
                child = React.cloneElement(this.props.children, {
                    actions: actions,
                    companies: businessman.companies
                });
                break;
            case 'FreeCompanies' :
                child = React.cloneElement(this.props.children, {
                    actions: actions,
                    companies: this.props.companies
                });
                break;
            case 'Company' :
                child = React.cloneElement(this.props.children, {
                    actions: actions,
                    company: this.props.company,
                    partners: this.props.partners
                });
                break;
        }

        let app = (
                ( Object.keys(businessman.info).length && businessman.info.firstName )
            ?
                <div id='main'>
                    <NavBar/>
                    <div className='top-strut'></div>
                    <div className='container'>
                        <div className='row main-row'>
                            <div className='col-md-4'>
                                <div className='fixed-wrapper'>
                                    <BusinessmanCard businessman={businessman.info}/>
                                    <TapeCard tape={this.props.tape} actions={actions} />
                                    <AppInfoCard/>
                                </div>
                            </div>
                            <div className='col-md-8'>
                                <div id='cash'>
                                    <Money className='cash' value={businessman.info.cash}/>
                                </div>
                                {child}
                            </div>
                        </div>
                    </div>
                </div>
            :
                <img src='/resources/image/loading.gif'/>
        );
        return (
            <div>
                {app}
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        businessman: state.businessman,
        companies: state.companies,
        company: state.company,
        partners: state.partners,
        tape: state.tape
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}