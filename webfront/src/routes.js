import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './containers/App'
import LoginPage from './containers/LoginPage'
import PersonalCompanies from './component/company/PersonalCompanies'
import FreeCompanies from './component/company/FreeCompanies'
import Company from './component/company/Company'

export const routes = (
    <div>
        <Route path='/' component={App}>
            <IndexRoute component={PersonalCompanies}/>
            <Route path='/searchCompanies' component={FreeCompanies}/>
            <Route path='/company/:inn' component={Company}/>
        </Route>
        <Route path='login' component={LoginPage}/>
    </div>
);