TRUNCATE public."Economic_type" CASCADE;
TRUNCATE public."Businessman" CASCADE;
TRUNCATE public."Company" CASCADE;
TRUNCATE public."Good" CASCADE;
TRUNCATE public."Asset" CASCADE;
TRUNCATE public."Product" CASCADE;
TRUNCATE public."Financial_Result" CASCADE;
TRUNCATE public."Compatible_types" CASCADE;

ALTER SEQUENCE general_sequence RESTART WITH 1;
ALTER SEQUENCE businessman_sequence RESTART WITH 1;
ALTER SEQUENCE inn_sequence RESTART WITH 1000000000;


INSERT INTO public."Economic_type" VALUES('10.51', 'Производство молока и молочной продукции');
INSERT INTO public."Economic_type" VALUES('10.71', 'Производство хлеба и мучных кондитерских изделий, тортов и пирожных недлительного хранения');
INSERT INTO public."Economic_type" VALUES('55.30', 'Деятельность ресторанов и кафе');
INSERT INTO public."Economic_type" VALUES('62.0', 'Разработка компьютерного программного обеспечения, консультационные услуги в данной области и другие сопутствующие услуги');
INSERT INTO public."Economic_type" VALUES('24.4', 'Производство фармацевтической продукции');

INSERT INTO public."Compatible_types" VALUES('10.71', '55.30');
INSERT INTO public."Compatible_types" VALUES('55.30', '10.71');
INSERT INTO public."Compatible_types" VALUES('10.51', '55.30');
INSERT INTO public."Compatible_types" VALUES('55.30', '10.51');

INSERT INTO public."Businessman"(id, first_name, last_name, email, password, cash, avatar) VALUES(nextval('businessman_sequence'), 'Александр', 'Буркин', 'bay-burkin@yandex.ru', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', 1000000, '/resources/image/businessman/burkin.jpg');
INSERT INTO public."Businessman"(id, first_name, last_name, cash) VALUES(nextval('businessman_sequence'), 'Роман', 'Насонов', 1000000);
INSERT INTO public."Businessman"(id, first_name, last_name, cash, avatar) VALUES(nextval('businessman_sequence'), 'Ис', 'Бычков', 200000, '/resources/image/businessman/Is.jpg');

INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Вимм-Билль-Данн', 100000, 0, '10.51', 'Москва, Дмитровское шоссе, 108, "Лианозовский молочный комбинат"', '/resources/image/company/wbd.png', '/resources/image/visit_card/back_wbd.jpg', 'То, что ты хочешь!');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Московский пекарь', 100000, 0, '10.71', 'Москва, 1-й Институтский проезд, 3/10', '/resources/image/company/pekar.png', '/resources/image/visit_card/back_pekar.jpg', 'Мы производим вкусные булочки и по привлекательной цене');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Кафе Пушкинъ', 100000, 0, '55.30', 'Москва, Тверской бульвар, 26/5', '/resources/image/company/pushkin.jpg', '/resources/image/visit_card/back_pushkin.jpg', 'В чужбине свято наблюдаю родной обычай старины...');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Булка', 100000, 0, '55.30', 'Москва, Большая Грузинская, 69', '/resources/image/company/bulka.png', '/resources/image/visit_card/back_bulka.jpg', '"Булка" продает только самые свежие булочки');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Шоколадница', 100000, 0, '55.30', 'Москва, улица Грузинский Вал, 26/1', '/resources/image/company/shocoladnica.jpg', '/resources/image/visit_card/back_coffee.jpg', 'Утреннее кофе - бодрое начало дня');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Burger King', 100000, 0, '55.30', 'Москва, МКАД, 28-й километр, 2, стр. 2', '/resources/image/company/burger_king.png', '/resources/image/visit_card/back_burgers.jpg', 'Отсутствие голода - свободный разум');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo, background, slogan) VALUES(nextval('inn_sequence'), 'Durex', 100000, 0, '24.4', 'Москва, Хлебозаводский проезд, 7с10', '/resources/image/company/durex.png', '/resources/image/visit_card/back_durex.jpg', 'Все лучшее - на хуй!');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo) VALUES(nextval('inn_sequence'), 'EPAM Systems', 1000000000, 0, '62.0', 'Москва, 9-я Радиальная, 2', '/resources/image/company/epam.png');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo) VALUES(nextval('inn_sequence'), 'Яндекс', 1000000000, 0, '62.0', 'Москва, Льва Толстого, 16', '/resources/image/company/yandex.png');
INSERT INTO public."Company"(inn, name, price, budget, economic_type, address, logo) VALUES(nextval('inn_sequence'), 'ВКонтакте', 1000000000, 0, '62.0', 'Санкт-Петербург, Невский проспект, 28', '/resources/image/company/vk.png');

INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000000, 'Соки J7', 'J7 - это соки и нектары с богатой линейкой вкусов', '/resources/image/good/J7.jpg');
INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000000, 'Детское питание "Агуша"', 'Соответствие высоким технологическим стандартам качества и безопасность продукции – главные условия при производстве детского питания «Агуша»', '/resources/image/good/agusha.png');
INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000000, 'Сыры "Ламбер"', '«Ламбер» производится в Алтайском крае с его уникальной горной природой и заливными лугами', '/resources/image/good/lamber.jpg');
INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000001, 'Кекс "Столичный"', 'Вкусный кекс без разрыхлителей', '/resources/image/good/keks.jpg');
INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000001, 'Маффин шоколадный', 'Маффин с добавлением шоколада из эквадорских какао-бобов', '/resources/image/good/maffin.jpg');
INSERT INTO public."Good"(id, company_inn, name, description, logo) VALUES(nextval('general_sequence'), 1000000001, 'Пирожное "Наполеон"', 'Именно к праздничному столу, в честь победы над Наполеоном в 1812 году, поварам удалось приготовить изысканное пирожное...', '/resources/image/good/napoleon.jpg');

INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000000, 1, 23, 0);
INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000000, 2, 12, 0);
INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000000, 3, 258, 0);
INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000001, 4, 10, 0);
INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000001, 5, 15, 0);
INSERT INTO public."Asset"(id, company_inn, good_id, prime_cost, count) VALUES(nextval('general_sequence'), 1000000001, 6, 20, 0);

INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000000, 1, 23, 0);
INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000000, 2, 12, 0);
INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000000, 3, 258, 0);
INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000001, 4, 10, 0);
INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000001, 5, 15, 0);
INSERT INTO public."Product"(id, company_inn, good_id, market_cost, count) VALUES(nextval('general_sequence'), 1000000001, 6, 20, 0);