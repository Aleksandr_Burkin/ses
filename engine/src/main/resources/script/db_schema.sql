-- CREATE DATABASE ses_database;
-- CREATE USER ses WITH password 'ses';
-- GRANT ALL privileges ON DATABASE ses_database TO ses

DROP SCHEMA public CASCADE;
CREATE SCHEMA public AUTHORIZATION ses;
GRANT ALL ON SCHEMA public TO ses;



-- Table: "Businessman"
CREATE TABLE "Businessman"
(
  id numeric(20,0) NOT NULL,
  first_name character varying(255),
  last_name character varying(255),
  email character varying(255),
  password character varying(1000),
  cash numeric(18,0),
  avatar character varying(255),
  CONSTRAINT businessman_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Businessman"
  OWNER TO ses;


-- Table: "Businessman_history"
CREATE TABLE "Businessman_history"
(
  id numeric(20,0) NOT NULL,
  businessman_id numeric(20,0),
  message character varying(500),
  date timestamp without time zone,
  CONSTRAINT businessman_history_pk PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE "Businessman_history"
OWNER TO ses;


-- Table: "Company"
CREATE TABLE "Company"
(
  inn numeric(10,0) NOT NULL,
  name character varying(255),
  owner numeric(10,0),
  price numeric(18,0),
  budget numeric(18,0),
  economic_type character varying(5),
  address character varying(255),
  logo character varying(255),
  background character varying(255),
  slogan character varying(255),
  CONSTRAINT "Company_pk" PRIMARY KEY (inn)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Company"
  OWNER TO ses;


-- Table: "Company_history"
CREATE TABLE "Company_history"
(
  id numeric(20,0) NOT NULL,
  company_inn numeric(20,0),
  message character varying(500),
  date timestamp without time zone,
  CONSTRAINT company_history_pk PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE "Company_history"
OWNER TO ses;


-- Table: "Economic_type"
CREATE TABLE "Economic_type"
(
  code character varying(5) NOT NULL,
  description character varying(255),
  CONSTRAINT economic_type_pk PRIMARY KEY (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Economic_type"
  OWNER TO ses;


-- Table: "Compatible_types"
CREATE TABLE "Compatible_types"
(
  src_code character(5),
  dst_code character(5)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Compatible_types"
  OWNER TO ses;



-- Table: "Good"
CREATE TABLE "Good"
(
  id numeric(20,0) NOT NULL,
  company_inn numeric(20,0),
  name character varying(500),
  description character varying(500),
  logo character varying(500),
  CONSTRAINT good_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Good"
  OWNER TO ses;


-- Table: "Asset"
CREATE TABLE "Asset"
(
  id numeric(20,0) NOT NULL,
  company_inn numeric(20,0),
  good_id numeric(20,0),
  prime_cost numeric(18,0),
  count numeric(18,0),
  CONSTRAINT asset_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Asset"
  OWNER TO ses;



-- Table: "Product"
CREATE TABLE "Product"
(
  id numeric(20,0) NOT NULL,
  company_inn numeric(20,0),
  good_id numeric(20,0),
  prime_cost numeric(18,0),
  market_cost numeric(18,0),
  count numeric(18,0),
  CONSTRAINT product_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Product"
  OWNER TO ses;


-- Table: "Deal"
CREATE TABLE "Deal"
(
    id numeric(20, 0) NOT NULL,
    owner numeric(20, 0) NOT NULL,
    agent numeric(20, 0) NOT NULL,
    amount numeric(20, 0) NOT NULL,
    CONSTRAINT deal_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Deal"
  OWNER TO ses;


-- Table: "Financial_Result"
CREATE TABLE "Financial_Result"
(
  id numeric(20,0) NOT NULL,
  company_inn numeric(10,0),
  overhead numeric(18,0),
  net_profit numeric(18,0),
  profitability numeric(4),
  CONSTRAINT financial_result_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Financial_Result"
  OWNER TO ses;


-- Constraint
ALTER TABLE "Businessman_history"
ADD CONSTRAINT businessman_history_businessman_fk FOREIGN KEY (businessman_id)
REFERENCES "Businessman" (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Company"
ADD CONSTRAINT company_businessman_fk FOREIGN KEY (owner)
REFERENCES "Businessman" (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Company"
ADD CONSTRAINT company_economic_type FOREIGN KEY (economic_type)
REFERENCES "Economic_type" (code) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Company_history"
ADD CONSTRAINT company_history_company_fk FOREIGN KEY (company_inn)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Good"
ADD CONSTRAINT good_company_fk FOREIGN KEY (company_inn)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Asset"
ADD CONSTRAINT asset_company_fk FOREIGN KEY (company_inn)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Asset"
ADD CONSTRAINT asset_good_fk FOREIGN KEY (good_id)
REFERENCES "Good" (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Product"
ADD CONSTRAINT product_company_fk FOREIGN KEY (company_inn)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Product"
ADD CONSTRAINT product_good_fk FOREIGN KEY (good_id)
REFERENCES "Good" (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Deal"
ADD CONSTRAINT deal_owner_fk FOREIGN KEY (owner)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Deal"
ADD CONSTRAINT deal_agent_fk FOREIGN KEY (agent)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "Financial_Result"
ADD CONSTRAINT financial_result_company_fk FOREIGN KEY (company_inn)
REFERENCES "Company" (inn) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;


-- Sequence: businessman_sequence
CREATE SEQUENCE businessman_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 1000000
  START 1
  CACHE 1;
ALTER TABLE businessman_sequence
  OWNER TO ses;


-- Sequence: inn_sequence
CREATE SEQUENCE inn_sequence
  INCREMENT 1
  MINVALUE 1000000000
  MAXVALUE 9999999999
  START 1000000000
  CACHE 1;
ALTER TABLE inn_sequence
  OWNER TO ses;


-- Sequence: general_sequence
CREATE SEQUENCE general_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 1000000
  START 1
  CACHE 1;
ALTER TABLE general_sequence
  OWNER TO ses;