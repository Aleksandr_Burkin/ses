package net.ses.engine.domain;

/**
 * Created by Aleksandr_Burkin on 05.05.17.
 */
public class CompanyHistory {

    private Long id;
    private Company company;
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
