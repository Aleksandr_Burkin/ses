package net.ses.engine.domain;

import net.ses.engine.core.Money;

/**
 * Created by Aleksandr_Burkin on 4/27/2017.
 *
 */
public class Good {

    private Long id;
    private Company company;
    private String name;
    private String description;
    private String logo;
    private Money primeCost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Money getPrimeCost() {
        return primeCost;
    }

    public void setPrimeCost(Money primeCost) {
        this.primeCost = primeCost;
    }
}
