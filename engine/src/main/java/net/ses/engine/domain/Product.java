package net.ses.engine.domain;

import net.ses.engine.core.Money;

/**
 * Created by Aleksandr_Burkin on 19.02.17.
 *
 */
public class Product {

    private Long id;
    private Company company;
    private Good good;
    private Money primeCost;
    private Money marketCost;
    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public Money getPrimeCost() {
        return primeCost;
    }

    public void setPrimeCost(Money primeCost) {
        this.primeCost = primeCost;
    }

    public Money getMarketCost() {
        return marketCost;
    }

    public void setMarketCost(Money marketCost) {
        this.marketCost = marketCost;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
