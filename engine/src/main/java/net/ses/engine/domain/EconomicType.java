package net.ses.engine.domain;

import java.util.Objects;

/**
 * Created by Roman Nasonov on 16.02.2017.
 * Тип предприятия по двухчисленной части кода ОКВЭД
 */
public class EconomicType {

    private String code;
    private String description;

    public EconomicType() {}

    public EconomicType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EconomicType that = (EconomicType) o;

        if (!code.equals(that.code)) return false;
        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.code, this.description);
    }

    @Override
    public String toString() {
        return "EconomicType{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
