package net.ses.engine.domain;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 *
 * На данном этапе класс необходим для работы токенов
 */
public class Rank {

    private String type = "DEFAULT";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
