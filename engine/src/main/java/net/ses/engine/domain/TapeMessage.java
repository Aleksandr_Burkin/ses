package net.ses.engine.domain;

import java.sql.Timestamp;

/**
 * Created by Aleksandr_Burkin on 09.05.17.
 */
public class TapeMessage {

    private String text;
    private Timestamp date;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
