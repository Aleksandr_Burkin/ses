package net.ses.engine.domain;

import net.ses.engine.core.Money;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 2/16/2017.
 *
 * Предприятие
 */
public class Company {

    private Long INN;
    private String name;
    private EconomicType economicType;
    private Businessman owner;
    private Money price;
    private Money budget;
    private String address;
    private String logo;
    private String background;
    private String slogan;
    private List<Product> products;
    private FinancialResult financialResult;

    public Company() {
        products = new ArrayList<>();
        budget = new Money(0L);
    }

    public Company(FinancialResult financialResult) {
        this();
        this.financialResult = financialResult;
    }

    /**
     *
     * @return ИНН (он же ID предприятия)
     */
    public Long getINN() {
        return INN;
    }

    public void setINN(Long INN) {
        this.INN = INN;
    }

    /**
     *
     * @return имя компании
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return экономический тип по ОКВЭД
     */
    public EconomicType getEconomicType() {
        return economicType;
    }

    public void setEconomicType(EconomicType economicType) {
        this.economicType = economicType;
    }

    /**
     *
     * @return владелец
     */
    public Businessman getOwner() {
        return owner;
    }

    public void setOwner(Businessman owner) {
        this.owner = owner;
    }

    /**
     *
     * @return рыночная стоимость
     */
    public Money getPrice() {
        return price;
    }

    public void setPrice(Money money) {
        this.price = money;
    }

    /**
     *
     * @return величина инвестиций в компанию
     */
    public Money getBudget() {
        return budget;
    }

    public void addBudget(Money budget) {
        this.budget = this.budget.add(budget);
    }

    /**
     *
     * @return юридический адрес предприятия
     */
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return относительный url логотипа
     */
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     *
     * @return относительный url фона
     */
    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    /**
     *
     * @return слоган компании
     */
    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    /**
     *
     * @return благо
     */
    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    public void removeGood(Product product) {
        this.products.remove(product);
    }

    /**
     *
     * @return денежный оборот
     */
    public FinancialResult getFinancialResult() {
        return financialResult;
    }

    public void setFinancialResult(FinancialResult financialResult) {
        this.financialResult = financialResult;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("INN = " + INN + ", ");
        builder.append("name = " + name + ", ");
        builder.append("address = " + address + ", ");
        builder.append("price = " + price);

        return builder.toString();
    }
}
