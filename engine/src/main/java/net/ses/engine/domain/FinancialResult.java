package net.ses.engine.domain;

import net.ses.engine.core.Money;

/**
 * Created by Aleksandr_Burkin on 18.02.17.
 *
 * Отчет о финансовых результатах компании (прибыль и расходы) за последний период
 */
public class FinancialResult {

    private Long id;
    private Company company;
    private Money overhead;
    private Money netProfit;
    private Integer profitability;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Money getOverhead() {
        return overhead;
    }

    public void setOverhead(Money overhead) {
        this.overhead = overhead;
    }

    public Money getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(Money netProfit) {
        this.netProfit = netProfit;
    }

    public Integer getProfitability() {
        return profitability;
    }

    public void setProfitability(Integer profitability) {
        this.profitability = profitability;
    }
}
