package net.ses.engine.domain;

/**
 * Created by Aleksandr_Burkin on 06.05.17.
 */
public class BusinessmanHistory {

    private Long id;
    private Businessman businessman;
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Businessman getBusinessman() {
        return businessman;
    }

    public void setBusinessman(Businessman businessman) {
        this.businessman = businessman;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
