package net.ses.engine.domain;

import net.ses.engine.core.Money;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 2/16/2017.
 *
 * Предприниматель
 */
public class Businessman {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Rank rank = new Rank(); //stub
    private Money cash;
    private String avatar;
    private List<Company> companies;

    public Businessman() {
        companies = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Money getCash() {
        return cash;
    }

    public void setCash(Money cash) {
        this.cash = cash;
    }

    public void addCash(Money money) {
        this.cash = this.cash.add(cash);
    }

    public void subtractCash(Money cash) {
        this.cash = this.cash.subtract(cash);
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void addCompany(Company company) {
        companies.add(company);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("id = " + id + ", ");
        builder.append("firstName = " + firstName + ", ");
        builder.append("lastName = " + lastName + ", ");
        builder.append("email = " + email + ", ");
        builder.append("money = " + cash);

        return builder.toString();
    }
}
