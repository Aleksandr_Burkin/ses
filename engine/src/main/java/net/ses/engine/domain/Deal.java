package net.ses.engine.domain;

import net.ses.engine.core.Money;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public class Deal {

    private Long id;
    private Company owner;
    private Company agent;
    private Money amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getOwner() {
        return owner;
    }

    public void setOwner(Company owner) {
        this.owner = owner;
    }

    public Company getAgent() {
        return agent;
    }

    public void setAgent(Company agent) {
        this.agent = agent;
    }

    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }
}
