package net.ses.engine.dao;

import net.ses.engine.domain.FinancialResult;

/**
 * Created by Aleksandr_Burkin on 07.05.17.
 */
public interface FinancialResultDao {

    void save(FinancialResult result);

    FinancialResult getLastByCompanyINN(Long inn);
}
