package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.DealDao;
import net.ses.engine.domain.Deal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public class DealDaoImpl extends NamedParameterJdbcDaoSupport implements DealDao {

    @Value(value = "${target.table.deal}")
    private String targetTable;

    @Override
    public void save(Deal deal) {
        String sql = "INSERT INTO " + targetTable + " (id, owner, agent, amount) " +
                "VALUES(nextval('general_sequence'), :owner, :agent, :amount)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("owner", deal.getOwner().getINN());
        params.addValue("agent", deal.getAgent().getINN());
        params.addValue("amount", deal.getAmount().getValue());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public List<Deal> getAllByCompanyINN(Long companyINN) {
        String sql = "SELECT * " +
                     "FROM " + targetTable + " d " +
                     "WHERE owner = " + companyINN;

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            Deal deal = new Deal();
            deal.setId(resultSet.getLong("id"));
            deal.setAmount(new Money(resultSet.getLong("amount")));

            return deal;
        });
    }
}
