package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.AssetDao;
import net.ses.engine.domain.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/27/2017.
 */
public class AssetDaoImpl extends NamedParameterJdbcDaoSupport implements AssetDao {

    @Value(value = "${target.table.asset}")
    private String targetTable;

    @Value(value = "${target.table.good}")
    private String goodTable;

    @Value(value = "${target.table.company}")
    private String companyTable;

    @Override
    public void save(Asset asset) {
        String sql = "INSERT INTO " + targetTable + " (id, company_inn, good_id, prime_cost, count) " +
                "VALUES(nextval('general_sequence'), :company_inn, :good_id, :prime_cost, :count)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", asset.getCompany().getINN());
        params.addValue("good_id", asset.getGood().getId());
        params.addValue("prime_cost", asset.getPrimeCost().getValue());
        params.addValue("count", asset.getCount());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public Asset getById(Long id) {
        String sql = "SELECT " +
                     "a.id, " +
                     "a.company_inn, " +
                     "a.good_id, " +
                     "a.prime_cost, " +
                     "a.count," +
                     "g.name, " +
                     "g.description, " +
                     "g.logo " +
                     "FROM "+ targetTable + " a " +
                     "LEFT JOIN " + goodTable + " g " +
                     "ON a.good_id = g.id " +
                     "WHERE a.id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        Asset asset = getNamedParameterJdbcTemplate().queryForObject(sql, params, (resultSet, i) -> {
            Asset entity = new Asset();
            entity.setId(resultSet.getLong("id"));
            Company company = new Company();
            company.setINN(resultSet.getLong("company_inn"));
            entity.setCompany(company);
            //TODO stub
            Good good = new Good();
            good.setId(resultSet.getLong("good_id"));
            good.setName(resultSet.getString("name"));
            good.setDescription(resultSet.getString("description"));
            good.setLogo(resultSet.getString("logo"));
            entity.setGood(good);
            entity.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            entity.setCount(resultSet.getLong("count"));

            return entity;
        });

        return asset;
    }

    @Override
    public List<Asset> getByCompanyINN(Long inn) {
        String sql = "SELECT " +
                     "a.id, " +
                     "a.company_inn, " +
                     "a.prime_cost, " +
                     "a.count, " +
                     "g.id good_id, " +
                     "g.company_inn copyright_inn, " +
                     "g.name, " +
                     "g.description, " +
                     "g.logo, " +
                     "c2.name copyright_name " +
                     "FROM " + targetTable + " a " +
                     "LEFT JOIN " + companyTable + " c1 " +
                     "ON a.company_inn = c1.inn " +
                     "LEFT JOIN " + goodTable + " g " +
                     "ON a.good_id = g.id " +
                     "LEFT JOIN " + companyTable + " c2 " +
                     "ON g.company_inn = c2.inn " +
                     "WHERE c1.inn = :inn";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", inn);
        return getNamedParameterJdbcTemplate().query(sql, params, (resultSet, i) -> {
            Asset asset = new Asset();
            asset.setId(resultSet.getLong("id"));
            asset.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            asset.setCount(resultSet.getLong("count"));

            Good good = new Good();
            good.setId(resultSet.getLong("good_id"));
            //TODO компанию из кэша
            Company copyright = new Company();
            copyright.setINN(resultSet.getLong("copyright_inn"));
            copyright.setName(resultSet.getString("copyright_name"));
            good.setCompany(copyright);
            good.setName(resultSet.getString("name"));
            good.setDescription(resultSet.getString("description"));
            good.setLogo(resultSet.getString("logo"));
            asset.setGood(good);

            return asset;
        });
    }

    @Override
    public Asset getByCompanyInnAndGoodId(Long companyINN, Long goodId) {
        String sql = "SELECT " +
                     "a.id, " +
                     "a.prime_cost, " +
                     "a.count " +
                     "FROM "+ targetTable +" a " +
                     "WHERE a.company_inn = :company_inn AND a.good_id = :good_id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", companyINN);
        params.addValue("good_id", goodId);

        List<Asset> list = getNamedParameterJdbcTemplate().query(sql, params, (resultSet, i) -> {
            Asset entity = new Asset();
            entity.setId(resultSet.getLong("id"));
            entity.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            entity.setCount(resultSet.getLong("count"));

            return entity;
        });

        if( list.isEmpty() )
            return null;

        return list.get(0);
    }

    @Override
    public void updateCount(Long assetId, Long count) {
        String sql = "UPDATE " + targetTable + " SET count = :count WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", assetId);
        params.addValue("count", count);

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public void delete(Long assetId) {
        String sql = "DELETE FROM " + targetTable + " WHERE id = ?";

        getJdbcTemplate().update(sql, assetId);
    }
}
