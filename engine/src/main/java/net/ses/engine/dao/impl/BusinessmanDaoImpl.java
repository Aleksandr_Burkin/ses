package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.BusinessmanDao;
import net.ses.engine.dao.CompanyDao;
import net.ses.engine.domain.Businessman;
import net.ses.engine.domain.Company;
import net.ses.engine.exception.NotEnoughMoneyException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by Aleksandr_Burkin on 3/1/2017.
 */
@Transactional
public class BusinessmanDaoImpl extends NamedParameterJdbcDaoSupport implements BusinessmanDao {

    private CompanyDao companyDao;

    @Value(value = "${target.table.businessman}")
    private String targetTable;

    @Value(value = "${target.table.company}")
    private String companyTable;

    public void setCompanyDao(CompanyDao companyDao) {
        this.companyDao = companyDao;
    }

    @Override
    public void save(Businessman entity) {
        String sql = "INSERT INTO " + targetTable + " (id, first_name, last_name, cash) " +
                "VALUES(nextval('businessman_sequence'), :first_name, :last_name, :cash)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("first_name", entity.getFirstName());
        params.addValue("last_name", entity.getLastName());
        params.addValue("cash", entity.getCash().getValue());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public void update(Businessman entity) {
        String sql = "UPDATE " + targetTable + " SET cash = :cash WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", entity.getId());
        params.addValue("cash", entity.getCash().getValue());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public Businessman getById(Long id) {
        String sql = "SELECT " +
                     "b.id, " +
                     "b.first_name, " +
                     "b.last_name, " +
                     "b.cash, " +
                     "b.avatar, " +
                     "c.inn, " +
                     "c.name, " +
                     "c.logo " +
                     "FROM " + targetTable + " b " +
                     "LEFT JOIN " + companyTable + " c " +
                     "ON b.id = c.owner WHERE b.id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        Businessman businessman = new Businessman();

        getNamedParameterJdbcTemplate().query(sql, params, resultSet -> {
            if( businessman.getId() == null ) {
                businessman.setId(resultSet.getLong("id"));
                businessman.setFirstName(resultSet.getString("first_name"));
                businessman.setLastName(resultSet.getString("last_name"));
                businessman.setCash(new Money(resultSet.getLong("cash")));
                businessman.setAvatar(resultSet.getString("avatar"));
            }
            if( resultSet.getString("name") != null ) {
                Company company = new Company();
                company.setINN(resultSet.getLong("inn"));
                company.setName(resultSet.getString("name"));
                company.setLogo(resultSet.getString("logo"));
                businessman.addCompany(company);
            }
        });

        return businessman;
    }

    @Override
    public Optional<Businessman> getByEmail(String email) {
        String sql = "SELECT " +
                     "b.id, " +
                     "b.email, " +
                     "b.password " +
                     "FROM "+ targetTable +" b " +
                     "WHERE b.email = :email";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("email", email);

        List<Businessman> list = getNamedParameterJdbcTemplate().query(sql, params, (resultSet, i) -> {
            Businessman businessman = new Businessman();
            businessman.setId(resultSet.getLong("id"));
            businessman.setEmail(resultSet.getString("email"));
            businessman.setPassword(resultSet.getString("password"));

            return businessman;
        });

        if( list.isEmpty() )
            return Optional.ofNullable(null);

        return Optional.ofNullable(list.get(0));
    }

    @Override
    public Money getCash(Long id) {
        String businessmanSql = "SELECT " +
                                "b.cash " +
                                "FROM "+ targetTable +" b " +
                                "WHERE b.id = :id";

        MapSqlParameterSource businessmanParams = new MapSqlParameterSource();
        businessmanParams.addValue("id", id);

        Businessman businessman = getNamedParameterJdbcTemplate().queryForObject(businessmanSql, businessmanParams, (resultSet, i) -> {
            Businessman entity = new Businessman();
            entity.setCash(new Money(resultSet.getLong("cash")));

            return entity;
        });

        return businessman.getCash();
    }

    @Override
    public void updateCash(Long id, Long amount) {
        String sql = "UPDATE " + targetTable + " SET cash = :cash WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        params.addValue("cash", amount);

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public void investToCompany(Long businessmanId, Long companyInn, Long amount) {
        Money money = getCash(businessmanId);

        if( money.getValue() < amount ) {
            throw new NotEnoughMoneyException("Сумма инвестиций больше, чем собственный капитал бизнесмена");
        }

        Money budget = companyDao.getBudget(companyInn);

        updateCash(businessmanId, money.subtract(new Money(amount)).getValue());
        companyDao.updateBudget(companyInn, budget.add(new Money(amount)).getValue());
    }
}
