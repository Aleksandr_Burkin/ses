package net.ses.engine.dao;

import net.ses.engine.domain.CompanyHistory;
import net.ses.engine.domain.TapeMessage;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 05.05.17.
 */
public interface CompanyHistoryDao {

    void save(CompanyHistory message);

    List<TapeMessage> getMessagesByCompanyINN(Long inn);
}
