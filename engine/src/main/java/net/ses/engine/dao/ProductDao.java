package net.ses.engine.dao;

import net.ses.engine.domain.Product;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public interface ProductDao {

    void save(Product good);

    Product getById(Long id);

    List<Product> getByCompanyINN(Long companyINN);

    Product getByCompanyInnAndGoodId(Long companyINN, Long goodId);

    void updateCount(Long productId, Long count);

    void updateMarketCost(Long productId, Long marketCost);
}
