package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.ProductDao;
import net.ses.engine.domain.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.List;
import java.util.Optional;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public class ProductDaoImpl extends NamedParameterJdbcDaoSupport implements ProductDao {

    @Value(value = "${target.table.product}")
    private String targetTable;

    @Value(value = "${target.table.good}")
    private String goodTable;

    @Value(value = "${target.table.asset}")
    private String assetTable;

    @Value(value = "${target.table.company}")
    private String companyTable;

    @Override
    public void save(Product product) {
        String sql = "INSERT INTO " + targetTable + " (id, company_inn, good_id, prime_cost, market_cost, count) " +
                "VALUES(nextval('general_sequence'), :company_inn, :good_id, :prime_cost, :market_cost, :count)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", product.getCompany().getINN());
        params.addValue("good_id", product.getGood().getId());
        params.addValue("prime_cost", product.getPrimeCost().getValue());
        params.addValue("market_cost", product.getPrimeCost().getValue());
        params.addValue("count", product.getCount());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public Product getById(Long id) {
        String sql = "SELECT " +
                     "p.id, " +
                     "p.company_inn, " +
                     "p.prime_cost, " +
                     "p.market_cost, " +
                     "p.count, " +
                     "g.id good_id, " +
                     "g.company_inn copyright, " +
                     "g.name, " +
                     "g.description, " +
                     "g.logo " +
                     "FROM " + targetTable + " p " +
                     "LEFT JOIN " + goodTable + " g " +
                     "ON p.good_id = g.id " +
                     "WHERE p.id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        return getNamedParameterJdbcTemplate().queryForObject(sql, params, (resultSet, i) -> {
            Product product = new Product();
            product.setId(resultSet.getLong("id"));

            //TODO компанию из кэша
            Company company = new Company();
            company.setINN(resultSet.getLong("company_inn"));
            product.setCompany(company);

            Good good = new Good();
            good.setId(resultSet.getLong("good_id"));
            //TODO компанию из кэша
            Company copyright = new Company();
            copyright.setINN(resultSet.getLong("copyright"));
            good.setCompany(copyright);
            good.setName(resultSet.getString("name"));
            good.setDescription(resultSet.getString("description"));
            good.setLogo(resultSet.getString("logo"));
            product.setGood(good);

            product.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            product.setMarketCost(new Money(resultSet.getLong("market_cost")));
            product.setCount(resultSet.getLong("count"));

            return product;
        });
    }

    @Override
    public List<Product> getByCompanyINN(Long inn) {
        String sql = "SELECT " +
                        "p.id, " +
                        "p.market_cost, " +
                        "p.count, " +
                        "p.prime_cost, " +
                        "g.name good_name, " +
                        "g.description good_description, " +
                        "g.logo good_logo " +
                     "FROM " + targetTable + " p " +
                     "LEFT JOIN " + goodTable + " g " +
                     "ON p.good_id = g.id " +
                     "LEFT JOIN " + companyTable + " c " +
                     "ON p.company_inn = c.inn " +
                     "WHERE c.inn = " + inn;

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            Product product = new Product();
            product.setId(resultSet.getLong("id"));
            product.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            product.setMarketCost(new Money(resultSet.getLong("market_cost")));
            product.setCount(resultSet.getLong("count"));

            Good good = new Good();
            good.setName(resultSet.getString("good_name"));
            good.setDescription(resultSet.getString("good_description"));
            good.setLogo(resultSet.getString("good_logo"));
            product.setGood(good);

            return product;
        });
    }

    @Override
    public Product getByCompanyInnAndGoodId(Long companyINN, Long goodId) {
        String sql = "SELECT " +
                     "p.id, " +
                     "p.prime_cost, " +
                     "p.market_cost, " +
                     "p.count " +
                     "FROM "+ targetTable +" p " +
                     "WHERE p.company_inn = :company_inn AND p.good_id = :good_id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", companyINN);
        params.addValue("good_id", goodId);

        List<Product> list = getNamedParameterJdbcTemplate().query(sql, params, (resultSet, i) -> {
            Product entity = new Product();
            entity.setId(resultSet.getLong("id"));
            entity.setPrimeCost(new Money(resultSet.getLong("prime_cost")));
            entity.setMarketCost(new Money(resultSet.getLong("market_cost")));
            entity.setCount(resultSet.getLong("count"));

            return entity;
        });

        if( list.isEmpty() )
            return null;

        return list.get(0);
    }

    @Override
    public void updateCount(Long productId, Long count) {
        String sql = "UPDATE " + targetTable + " SET count = :count WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", productId);
        params.addValue("count", count);

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public void updateMarketCost(Long productId, Long marketCost) {
        String sql = "UPDATE " + targetTable + " SET market_cost = :market_cost WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", productId);
        params.addValue("market_cost", marketCost);

        getNamedParameterJdbcTemplate().update(sql, params);
    }
}
