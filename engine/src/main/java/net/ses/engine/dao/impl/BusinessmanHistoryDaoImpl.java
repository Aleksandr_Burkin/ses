package net.ses.engine.dao.impl;

import net.ses.engine.dao.BusinessmanHistoryDao;
import net.ses.engine.domain.BusinessmanHistory;
import net.ses.engine.domain.TapeMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 06.05.17.
 */
public class BusinessmanHistoryDaoImpl extends NamedParameterJdbcDaoSupport implements BusinessmanHistoryDao {

    @Value(value = "${target.table.businessman.history}")
    private String targetTable;

    @Override
    public void save(BusinessmanHistory message) {
        String sql = "INSERT INTO " + targetTable + " (id, businessman_id, message, date) " +
                     "VALUES(nextval('general_sequence'), :businessman_id, :message, :date)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("businessman_id", message.getBusinessman().getId());
        params.addValue("message", message.getMessage());
        params.addValue("date", new Date());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public List<TapeMessage> getMessagesByBusinessmanId(Long id) {
        String sql = "SELECT " +
                     "b.message, " +
                     "b.date " +
                     "FROM " + targetTable + " b " +
                     "WHERE businessman_id = " + id +
                     " ORDER BY id DESC";

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            TapeMessage message = new TapeMessage();
            message.setText(resultSet.getString("message"));
            message.setDate(resultSet.getTimestamp("date"));

            return message;
        });
    }
}
