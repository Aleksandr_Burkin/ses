package net.ses.engine.dao;

import net.ses.engine.core.Money;
import net.ses.engine.domain.Businessman;

import java.util.Optional;

/**
 * Created by Aleksandr_Burkin on 3/1/2017.
 */
public interface BusinessmanDao {

    void save(Businessman entity);

    void update(Businessman entity);

    Businessman getById(Long id);

    Optional<Businessman> getByEmail(String email);

    Money getCash(Long businessmanId);

    void updateCash(Long id, Long amount);

    void investToCompany(Long businessmanId, Long companyInn, Long amount);
}
