package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.CompanyDao;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.EconomicType;
import net.ses.engine.domain.FinancialResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Aleksandr_Burkin on 23.02.17.
 *
 */
@Transactional
public class CompanyDaoImpl extends NamedParameterJdbcDaoSupport implements CompanyDao {

    @Value(value = "${target.table.company}")
    private String targetTable;

    @Value(value = "${target.table.economic.type}")
    private String economicTypeTable;

    @Value(value = "${target.table.good}")
    private String goodTable;

    @Value(value = "${target.table.product}")
    private String productTable;

    @Value(value = "${target.table.financial.result}")
    private String financialResultTable;

    @Value(value = "${target.table.compatible.types}")
    private String compatibleTypeTable;

    @Override
    public void save(Company company) {
        String sql = "INSERT INTO " + targetTable + " (inn, name, general_type, economic_type, owner, price, address) " +
                     "VALUES(nextval('inn_sequence'), :name, :general_type, :economic_type, :owner, :price, :address)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", company.getName());
        params.addValue("general_type", null);
        params.addValue("economic_type", null);
        params.addValue("owner", null);
        params.addValue("price", company.getPrice().getValue());
        params.addValue("address", company.getAddress());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public void update(Company entity) {
        String sql = "UPDATE " + targetTable + " SET owner = :owner WHERE inn = :inn";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", entity.getINN());
        params.addValue("owner", entity.getOwner().getId());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public Company getByINN(Long inn) {
        String sql = "SELECT * FROM " + targetTable  + " c " +
                    "JOIN " + economicTypeTable + " e " +
                    "ON c.economic_type = e.code " +
                    "LEFT JOIN (SELECT * FROM " + financialResultTable + " ORDER BY id DESC LIMIT 1) f " +
                    "ON c.inn = f.company_inn " +
                    "WHERE inn = :inn";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", inn);

        Company company = new Company();

        getNamedParameterJdbcTemplate().query(sql, params, resultSet -> {
            company.setINN(resultSet.getLong("inn"));
            company.setName(resultSet.getString("name"));
            company.setPrice(new Money(resultSet.getLong("price")));
            company.addBudget(new Money(resultSet.getLong("budget")));
            company.setAddress(resultSet.getString("address"));
            company.setLogo(resultSet.getString("logo"));
            company.setBackground(resultSet.getString("background"));
            company.setSlogan(resultSet.getString("slogan"));

            //TODO economicType в кэш
            EconomicType economicType = new EconomicType();
            economicType.setCode(resultSet.getString("code"));
            economicType.setDescription(resultSet.getString("description"));

            company.setEconomicType(economicType);
        });

        return company;
    }

    @Override
    public void delete(Company company) {
        String sql = "DELETE FROM " + targetTable + " WHERE inn = ?";

        getJdbcTemplate().update(sql, company.getINN());
    }


    @Override
    public List<Company> getFreeCompanies() {
        String sql = "SELECT * " +
                     "FROM " + targetTable + " c " +
                     "JOIN " + economicTypeTable + " e " +
                     "ON c.economic_type = e.code " +
                     "WHERE owner IS NULL";

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            Company company = new Company();
            company.setINN(resultSet.getLong("inn"));
            company.setName(resultSet.getString("name"));
            company.setPrice(new Money(resultSet.getLong("price")));
            company.setAddress(resultSet.getString("address"));
            company.setLogo(resultSet.getString("logo"));

            //TODO в кэш
            EconomicType economicType = new EconomicType();
            economicType.setCode(resultSet.getString("code"));
            economicType.setDescription(resultSet.getString("description"));

            company.setEconomicType(economicType);

            return company;
        });
    }

    @Override
    public Money getBudget(Long inn) {
        String companySql = "SELECT " +
                            "c.budget " +
                            "FROM "+ targetTable +" c " +
                            "WHERE c.inn = :inn";

        MapSqlParameterSource companyParams = new MapSqlParameterSource();
        companyParams.addValue("inn", inn);

        Company company = getNamedParameterJdbcTemplate().queryForObject(companySql, companyParams, (resultSet, i) -> {
            Company entity = new Company();
            entity.addBudget(new Money(resultSet.getLong("budget")));

            return entity;
        });

        return company.getBudget();
    }

    @Override
    public void updateBudget(Long inn, Long amount) {
        String sql = "UPDATE " + targetTable + " SET budget = :budget WHERE inn = :inn";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", inn);
        params.addValue("budget", amount);

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    /**
     * Возвращает совместимые компании по ОКВЭД (потенциальные партнеры)
     */
    @Override
    public List<Company> getCompatibleCompanies(Long inn) {
        String sql = "SELECT " +
                         "c2.inn, " +
                         "c2.name, " +
                         "c2.owner, " +
                         "c2.budget, " +
                         "c2.address, " +
                         "c2.logo, " +
                         "c2.background, " +
                         "c2.slogan, " +
                         "c2.economic_type, " +
                         "et.description " +
                     "FROM " + targetTable + " c1 " +
                     "LEFT JOIN " + compatibleTypeTable + " ct " +
                     "ON c1.economic_type = ct.src_code " +
                     "LEFT JOIN " + targetTable + " c2 " +
                     "ON c2.economic_type = ct.dst_code " +
                     "LEFT JOIN " + economicTypeTable + " et " +
                     "ON c2.economic_type = et.code " +
                     "WHERE c1.inn = :inn";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", inn);

        List<Company> list = new ArrayList<>();
        getNamedParameterJdbcTemplate().query(sql, params, resultSet -> {
                Company company = new Company();
                company.setINN(resultSet.getLong("inn"));
                company.setName(resultSet.getString("name"));
                company.setEconomicType(new EconomicType(resultSet.getString("economic_type"), resultSet.getString("description")));
                company.setAddress(resultSet.getString("address"));
                company.setLogo(resultSet.getString("logo"));
                company.setBackground(resultSet.getString("background"));
                company.setSlogan(resultSet.getString("slogan"));
                list.add(company);
        });

        return list;
    }

    @Override
    public List<Company> getCompanyByOwner(Long businessmanId) {
        String sql = "SELECT " +
                     "c.inn, " +
                     "c.name, " +
                     "c.logo " +
                     "FROM " + targetTable + " c " +
                     "WHERE owner = " + businessmanId;

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            Company company = new Company();
            company.setINN(resultSet.getLong("inn"));
            company.setName(resultSet.getString("name"));
            company.setLogo(resultSet.getString("logo"));

            return company;
        });
    }
}
