package net.ses.engine.dao;

import net.ses.engine.core.Money;
import net.ses.engine.domain.Company;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 23.02.17.
 */
public interface CompanyDao {

    void save(Company company);

    void update(Company company);

    Company getByINN(Long inn);

    void delete(Company company);

    /**
     *
     * @return список неприобретенных (свободных) компаний
     */
    List<Company> getFreeCompanies();

    Money getBudget(Long inn);

    void updateBudget(Long inn, Long amount);

    List<Company> getCompatibleCompanies(Long inn);

    List<Company> getCompanyByOwner(Long businessmanId);
}
