package net.ses.engine.dao.impl;

import net.ses.engine.core.Money;
import net.ses.engine.dao.FinancialResultDao;
import net.ses.engine.domain.FinancialResult;
import net.ses.engine.domain.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 07.05.17.
 */
public class FinancialResultDaoImpl extends NamedParameterJdbcDaoSupport implements FinancialResultDao {

    @Value(value = "${target.table.financial.result}")
    private String targetTable;

    @Override
    public void save(FinancialResult result) {
        String sql = "INSERT INTO " + targetTable + " (id, company_inn, overhead, net_profit, profitability) " +
                     "VALUES(nextval('general_sequence'), :company_inn, :overhead, :net_profit, :profitability)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", result.getCompany().getINN());
        params.addValue("overhead", result.getOverhead().getValue());
        params.addValue("net_profit", result.getNetProfit().getValue());
        params.addValue("profitability", result.getProfitability());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public FinancialResult getLastByCompanyINN(Long inn) {
        String sql = "SELECT " +
                     "f.overhead, " +
                     "f.net_profit, " +
                     "f.profitability " +
                     "FROM " + targetTable + " f " +
                     "WHERE f.company_inn = :inn " +
                     "ORDER BY id DESC LIMIT 1";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", inn);

        List<FinancialResult> list = getNamedParameterJdbcTemplate().query(sql, params, (resultSet, i) -> {
            FinancialResult result = new FinancialResult();
            result.setOverhead(new Money(resultSet.getLong("overhead")));
            result.setNetProfit(new Money(resultSet.getLong("net_profit")));
            result.setProfitability(resultSet.getInt("profitability"));

            return result;
        });

        if( list.isEmpty() )
            return null;

        return list.get(0);
    }
}
