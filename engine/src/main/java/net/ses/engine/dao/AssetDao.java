package net.ses.engine.dao;

import net.ses.engine.domain.Asset;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/27/2017.
 */
public interface AssetDao {

    void save(Asset resource);

    Asset getById(Long id);

    List<Asset> getByCompanyINN(Long companyINN);

    Asset getByCompanyInnAndGoodId(Long companyINN, Long goodId);

    void updateCount(Long assetId, Long count);

    void delete(Long id);
}
