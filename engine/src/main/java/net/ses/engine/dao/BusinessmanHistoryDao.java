package net.ses.engine.dao;

import net.ses.engine.domain.BusinessmanHistory;
import net.ses.engine.domain.TapeMessage;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 06.05.17.
 */
public interface BusinessmanHistoryDao {

    void save(BusinessmanHistory message);

    List<TapeMessage> getMessagesByBusinessmanId(Long id);
}
