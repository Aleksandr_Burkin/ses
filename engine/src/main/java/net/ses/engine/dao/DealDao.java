package net.ses.engine.dao;

import net.ses.engine.domain.Deal;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public interface DealDao {

    void save(Deal deal);

    List<Deal> getAllByCompanyINN(Long companyINN);
}
