package net.ses.engine.dao.impl;

import net.ses.engine.dao.CompanyHistoryDao;
import net.ses.engine.domain.CompanyHistory;
import net.ses.engine.domain.TapeMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 05.05.17.
 */
public class CompanyHistoryDaoImpl extends NamedParameterJdbcDaoSupport implements CompanyHistoryDao {

    @Value(value = "${target.table.company.history}")
    private String targetTable;

    @Override
    public void save(CompanyHistory message) {
        String sql = "INSERT INTO " + targetTable + " (id, company_inn, message, date) " +
                     "VALUES(nextval('general_sequence'), :company_inn, :message, :date)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("company_inn", message.getCompany().getINN());
        params.addValue("message", message.getMessage());
        params.addValue("date", new Date());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public List<TapeMessage> getMessagesByCompanyINN(Long inn) {
        String sql = "SELECT " +
                     "c.message, " +
                     "c.date " +
                     "FROM " + targetTable + " c " +
                     "WHERE company_inn = " + inn +
                     " ORDER BY id DESC";

        return getJdbcTemplate().query(sql, (resultSet, i) -> {
            TapeMessage message = new TapeMessage();
            message.setText(resultSet.getString("message"));
            message.setDate(resultSet.getTimestamp("date"));

            return message;
        });
    }
}
