package net.ses.engine.service;

import net.ses.engine.core.Money;
import net.ses.engine.dao.AssetDao;
import net.ses.engine.dao.CompanyDao;
import net.ses.engine.dao.DealDao;
import net.ses.engine.dao.ProductDao;
import net.ses.engine.domain.Asset;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.Deal;
import net.ses.engine.domain.Product;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 29.04.17.
 */
public class ProductService {

    private ProductDao productDao;
    private CompanyDao companyDao;
    private AssetDao assetDao;
    private DealDao dealDao;

    public ProductService(ProductDao productDao, CompanyDao companyDao, AssetDao assetDao, DealDao dealDao) {
        this.productDao = productDao;
        this.companyDao = companyDao;
        this.assetDao = assetDao;
        this.dealDao = dealDao;
    }

    public List<Product> getByCompanyINN(Long inn) {
        return productDao.getByCompanyINN(inn);
    }

    public void buyProduct(Long toCompanyInn, Long productId, Long count) {
        Product product = productDao.getById(productId);

        Company toCompany = new Company();
        toCompany.setINN(toCompanyInn);

        Asset asset = assetDao.getByCompanyInnAndGoodId(toCompanyInn, product.getGood().getId());
        if( asset != null ) {
            assetDao.updateCount(asset.getId(), asset.getCount() + count);
        } else {
            asset = new Asset();

            asset.setCompany(toCompany);
            asset.setGood(product.getGood());
            asset.setPrimeCost(product.getMarketCost());
            asset.setCount(count);

            assetDao.save(asset);
        }
        Money budget = companyDao.getBudget(toCompanyInn);
        Money amount = asset.getPrimeCost().multiply(asset.getCount());
        Money newBudget = budget.subtract(amount);
        companyDao.updateBudget(toCompanyInn, newBudget.getValue());

        Deal deal = new Deal();
        deal.setOwner(toCompany);
        deal.setAgent(product.getCompany());
        deal.setAmount(amount);

        dealDao.save(deal);
    }

    public void changeMarketCost(Long productId, Long marketCost) {
        productDao.updateMarketCost(productId, marketCost);
    }

    public Product getById(Long productId) {
        return productDao.getById(productId);
    }
}
