package net.ses.engine.service;

import net.ses.engine.dao.AssetDao;
import net.ses.engine.dao.ProductDao;
import net.ses.engine.domain.Asset;
import net.ses.engine.domain.Product;
import net.ses.engine.exception.IncorrectCountValueException;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 27.04.17.
 */
public class AssetService {

    private AssetDao assetDao;

    private ProductDao productDao;

    public AssetService(AssetDao assetDao, ProductDao productDao) {
        this.assetDao = assetDao;
        this.productDao = productDao;
    }

    public List<Asset> getByCompanyINN(Long inn) {
        return assetDao.getByCompanyINN(inn);
    }

    public Long intoProducts(Long companyInn, Long assetId, Long count) {
        Asset asset = assetDao.getById(assetId);
        Long currentCount = asset.getCount();

        if( currentCount < count )
            throw new IncorrectCountValueException("Недостаточное количество активов");
        else if( currentCount.equals(count) )
            assetDao.delete(assetId);
        else
            assetDao.updateCount(assetId, currentCount - count);

        Product currentProduct = productDao.getByCompanyInnAndGoodId(companyInn, asset.getGood().getId());
        if( currentProduct != null ) {
            productDao.updateCount(currentProduct.getId(), currentProduct.getCount() + count);
        } else {
            Product product = new Product();
            product.setCompany(asset.getCompany());
            product.setGood(asset.getGood());
            product.setPrimeCost(asset.getPrimeCost());
            product.setMarketCost(asset.getPrimeCost());
            product.setCount(count);

            productDao.save(product);
        }

        return currentCount - count;
    }

    public Asset getById(Long id) {
        return assetDao.getById(id);
    }
}
