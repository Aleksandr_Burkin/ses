package net.ses.engine.service;

import net.ses.engine.core.Money;
import net.ses.engine.dao.BusinessmanDao;
import net.ses.engine.dao.BusinessmanHistoryDao;
import net.ses.engine.dao.CompanyDao;
import net.ses.engine.domain.Businessman;
import net.ses.engine.domain.BusinessmanHistory;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.TapeMessage;

import java.util.List;
import java.util.Optional;

/**
 * Created by Aleksandr_Burkin on 18.02.17.
 *
 * Сервис бизнесмена
 */
public class BusinessmanService {

    private BusinessmanDao businessmanDao;
    private BusinessmanHistoryDao businessmanHistoryDao;
    private CompanyDao companyDao;

    public BusinessmanService(BusinessmanDao businessmanDao, BusinessmanHistoryDao businessmanHistoryDao, CompanyDao companyDao) {
        this.businessmanDao = businessmanDao;
        this.businessmanHistoryDao =businessmanHistoryDao;
        this.companyDao = companyDao;
    }

    public Businessman getBusinessmanById(Long id) {
        return businessmanDao.getById(id);
    }

    public Optional<Businessman> getBusinessmanByEmail(String email) {
        return businessmanDao.getByEmail(email);
    }

    public void buyCompany(Long businessmanId, Long companyINN) {
        Businessman businessman = businessmanDao.getById(businessmanId);
        Company company = companyDao.getByINN(companyINN);

        businessman.subtractCash(company.getPrice());
        businessman.addCompany(company);
        company.setOwner(businessman);

        businessmanDao.update(businessman);
        companyDao.update(company);
    }

    public void investToCompany(Long businessmanId, Long companyInn, Money amount) {
        businessmanDao.investToCompany(businessmanId, companyInn, amount.getValue());
    }

    public Money getCash(Long businessmanId) {
        return businessmanDao.getCash(businessmanId);
    }

    public void saveMessage(Long businessmanId, String message) {
        BusinessmanHistory businessmanHistory = new BusinessmanHistory();
        Businessman businessman = new Businessman();
        businessman.setId(businessmanId);
        businessmanHistory.setBusinessman(businessman);
        businessmanHistory.setMessage(message);

        this.businessmanHistoryDao.save(businessmanHistory);
    }

    public List<TapeMessage> getMessagesByBusinessmanId(Long id) {
        return this.businessmanHistoryDao.getMessagesByBusinessmanId(id);
    }
}
