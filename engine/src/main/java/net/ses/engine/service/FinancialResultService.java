package net.ses.engine.service;

import net.ses.engine.core.Money;
import net.ses.engine.dao.FinancialResultDao;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.Deal;
import net.ses.engine.domain.FinancialResult;
import net.ses.engine.domain.Product;
import net.ses.engine.service.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import javax.servlet.ServletContext;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 04.05.17.
 */
public class FinancialResultService {

    @Autowired
    private ServletContext context;

    @Autowired
    private DealService dealService;

    @Autowired
    private ProductService productService;

    private FinancialResultDao financialResultDao;

    public FinancialResultService(FinancialResultDao financialResultDao) {
        this.financialResultDao = financialResultDao;
    }

    public void refresh() {
        Long companyInn = (Long)context.getAttribute("company_inn");

        FinancialResult result = new FinancialResult();

        Company company = new Company();
        company.setINN(companyInn);
        result.setCompany(company);

        List<Deal> deals = dealService.getAllByCompanyINN(companyInn);
        Long overhead = 0L;
        for(Deal deal : deals) {
            overhead += deal.getAmount().getValue();
        }
        result.setOverhead(new Money(overhead));

        //TODO stub
        List<Product> products = productService.getByCompanyINN(companyInn);
        if( products.size() == 0 ) return;
        Long netProfit = 0L;
        for(Product product : products) {
            Money primeCost = product.getPrimeCost();
            Money marketCost = product.getMarketCost();
            Long count = product.getCount();
            netProfit += marketCost.subtract(primeCost).getValue() * count;
        }
        result.setNetProfit(new Money(netProfit));

        result.setProfitability(10);

        financialResultDao.save(result);
    }

    public FinancialResult getLastResult() {
        Long companyInn = (Long)context.getAttribute("company_inn");

        return financialResultDao.getLastByCompanyINN(companyInn);
    }
}
