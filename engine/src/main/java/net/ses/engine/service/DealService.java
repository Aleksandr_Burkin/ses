package net.ses.engine.service;

import net.ses.engine.dao.*;
import net.ses.engine.domain.Deal;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 4/26/2017.
 */
public class DealService {

    private DealDao dealDao;

    public DealService(DealDao dealDao) {
        this.dealDao = dealDao;
    }

    public void save(Deal deal) {
        dealDao.save(deal);
    }

    public List<Deal> getAllByCompanyINN(Long inn) {
        return dealDao.getAllByCompanyINN(inn);
    }
}
