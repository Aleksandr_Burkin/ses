package net.ses.engine.service;

import net.ses.engine.core.Money;
import net.ses.engine.dao.CompanyDao;
import net.ses.engine.dao.CompanyHistoryDao;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.CompanyHistory;
import net.ses.engine.domain.TapeMessage;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 18.02.17.
 *
 * Сервис предприятия
 */
public class CompanyService {

    private CompanyDao companyDao;

    private CompanyHistoryDao companyHistoryDao;

    public CompanyService(CompanyDao companyDao, CompanyHistoryDao companyHistoryDao) {
        this.companyDao = companyDao;
        this.companyHistoryDao = companyHistoryDao;
    }

    public Company getByINN(Long inn) {
        return companyDao.getByINN(inn);
    }

    public List<Company> getFreeCompanies() {
        return companyDao.getFreeCompanies();
    }

    public Money getBudget(Long companyInn) {
        return companyDao.getBudget(companyInn);
    }

    public List<Company> getCompatibleCompanies(Long inn) {
        return companyDao.getCompatibleCompanies(inn);
    }

    public List<Company> getCompanyByOwner(Long businessmanId) {
        return companyDao.getCompanyByOwner(businessmanId);
    }

    public void saveMessage(Long companyINN, String message) {
        CompanyHistory companyHistory = new CompanyHistory();
        Company company = new Company();
        company.setINN(companyINN);
        companyHistory.setCompany(company);
        companyHistory.setMessage(message);

        this.companyHistoryDao.save(companyHistory);
    }

    public List<TapeMessage> getMessagesByCompanyINN(Long inn) {
        return this.companyHistoryDao.getMessagesByCompanyINN(inn);
    }
}
