package net.ses.engine.security.model;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public enum Scopes {
    REFRESH_TOKEN;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
