package net.ses.engine.security.model.token;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public interface JwtToken {
    String getToken();
}
