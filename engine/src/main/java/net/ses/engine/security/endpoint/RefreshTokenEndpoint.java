package net.ses.engine.security.endpoint;

import net.ses.engine.security.auth.jwt.extractor.TokenExtractor;
import net.ses.engine.security.auth.jwt.verifier.TokenVerifier;
import net.ses.engine.security.config.JwtSettings;
import net.ses.engine.security.config.WebSecurityConfig;
import net.ses.engine.security.exception.InvalidJwtToken;
import net.ses.engine.security.model.UserContext;
import net.ses.engine.security.model.token.JwtToken;
import net.ses.engine.security.model.token.JwtTokenFactory;
import net.ses.engine.security.model.token.RawAccessJwtToken;
import net.ses.engine.security.model.token.RefreshToken;
import net.ses.engine.domain.Businessman;
import net.ses.engine.service.BusinessmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
@RestController
public class RefreshTokenEndpoint {
    @Autowired
    private JwtTokenFactory tokenFactory;
    @Autowired
    private JwtSettings jwtSettings;
    @Autowired
    private BusinessmanService businessmanService;
    @Autowired
    private TokenVerifier tokenVerifier;
    @Autowired
    @Qualifier("jwtHeaderTokenExtractor") private TokenExtractor tokenExtractor;
    
    @RequestMapping(value="/api/auth/token", method=RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody JwtToken refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));
        
        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshToken.getSubject();
        Businessman businessman = businessmanService.getBusinessmanByEmail(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

        if (businessman.getRank() == null ) throw new InsufficientAuthenticationException("Businessman has no roles assigned");

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(businessman.getRank().getType()));

        UserContext userContext = UserContext.create(businessman.getEmail(), authorities);

        return tokenFactory.createAccessJwtToken(userContext);
    }
}
