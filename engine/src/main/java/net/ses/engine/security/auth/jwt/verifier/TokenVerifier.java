package net.ses.engine.security.auth.jwt.verifier;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public interface TokenVerifier {
    boolean verify(String jti);
}
