package net.ses.engine.security.config;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Created by Aleksandr_Burkin on 26.03.17.
 */
@Component
public class WebCorsConfigurationSource extends UrlBasedCorsConfigurationSource {

    public WebCorsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("http://localhost:9000");
        config.addAllowedMethod(HttpMethod.GET);
        config.addAllowedMethod(HttpMethod.POST);
        config.addAllowedMethod(HttpMethod.PUT);
        config.addAllowedHeader("*");
        config.setAllowCredentials(true);
        config.setMaxAge(360000L);

        this.registerCorsConfiguration("/**", config);
    }
}
