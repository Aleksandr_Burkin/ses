package net.ses.engine.security.auth.jwt.extractor;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public interface TokenExtractor {
    String extract(String payload);
}
