package net.ses.engine.security.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public class AuthMethodNotSupportedException extends AuthenticationServiceException {
    private static final long serialVersionUID = 3705043083010304496L;

    public AuthMethodNotSupportedException(String msg) {
        super(msg);
    }
}
