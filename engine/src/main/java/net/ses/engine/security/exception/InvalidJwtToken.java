package net.ses.engine.security.exception;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public class InvalidJwtToken extends RuntimeException {
    private static final long serialVersionUID = -294671188037098603L;
}
