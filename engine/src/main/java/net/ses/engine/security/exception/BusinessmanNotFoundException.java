package net.ses.engine.security.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
public class BusinessmanNotFoundException extends UsernameNotFoundException {

    public BusinessmanNotFoundException(String msg) {
        super(msg);
    }

    public BusinessmanNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}
