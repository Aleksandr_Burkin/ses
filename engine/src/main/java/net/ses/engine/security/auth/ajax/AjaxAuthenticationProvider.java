package net.ses.engine.security.auth.ajax;

import net.ses.engine.security.exception.BusinessmanNotFoundException;
import net.ses.engine.security.model.UserContext;
import net.ses.engine.domain.Businessman;
import net.ses.engine.service.BusinessmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr_Burkin on 25.03.17.
 */
@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private BusinessmanService businessmanService;
    @Autowired
    private ServletContext context;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String email = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        Businessman businessman = businessmanService.getBusinessmanByEmail(email)
                                    .orElseThrow(() -> new BusinessmanNotFoundException(
                                        "Businessman not found: nonexistent email (" + email + ")"));

        if (!encoder.matches(password, businessman.getPassword())) {
            throw new BadCredentialsException("Authentication Failed: password not valid.");
        }

        if (businessman.getRank() == null ) throw new InsufficientAuthenticationException("Businessman has no roles assigned");

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(businessman.getRank().getType()));

        UserContext userContext = UserContext.create(businessman.getEmail(), authorities);

        //TODO подумать как еще записать в контекст id пользователя
        context.setAttribute("businessman_id", businessman.getId());

        return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
