package net.ses.engine.task;

import net.ses.engine.domain.FinancialResult;
import net.ses.engine.service.FinancialResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by Aleksandr_Burkin on 04.05.17.
 */
public class FinancialResultScheduler {

    @Autowired
    private FinancialResultService refresher;

    @Autowired
    private SimpMessagingTemplate template;

    public void sendToClients(FinancialResult result) {
        if( result == null ) return;

        System.out.println("--> FinancialResult updated ");
        String message = "{\"overhead\": " + result.getOverhead().getValue() + ", " +
                          "\"netProfit\": " + result.getNetProfit().getValue() + "," +
                          "\"profitability\": " + result.getProfitability() + "}";
        this.template.convertAndSend("/topic/financialResult", message);
    }

    @Scheduled(fixedDelay=60000)
    public void run() {
        refresher.refresh();
        sendToClients(refresher.getLastResult());
    }
}
