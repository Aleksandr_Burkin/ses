package net.ses.engine.controller.dto.tape.mapping;

import net.ses.engine.controller.dto.tape.TapeMessageDTO;
import net.ses.engine.domain.TapeMessage;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 5/12/2017.
 */
public class TapeMessageMap extends PropertyMap<TapeMessage, TapeMessageDTO> {

    @Override
    protected void configure() {
        map().setDate(source.getDate());
    }
}
