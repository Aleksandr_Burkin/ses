package net.ses.engine.controller;

import net.ses.engine.controller.dto.company.CompanyDTO;
import net.ses.engine.controller.dto.company.FreeCompanyDTO;
import net.ses.engine.controller.dto.company.PartnerDTO;
import net.ses.engine.controller.dto.company.PersonalCompanyDTO;
import net.ses.engine.controller.dto.company.mapping.FreeCompanyMap;
import net.ses.engine.controller.dto.tape.TapeDTO;
import net.ses.engine.controller.dto.company.mapping.CompanyMap;
import net.ses.engine.controller.dto.company.mapping.PartnerMap;
import net.ses.engine.controller.dto.tape.TapeMessageDTO;
import net.ses.engine.controller.dto.tape.mapping.TapeMessageMap;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.TapeMessage;
import net.ses.engine.service.CompanyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aleksandr_Burkin on 02.03.17.
 */
@RestController
@RequestMapping(value = "/api")
public class CompanyController {

    @Autowired
    private ServletContext context;

    @Autowired
    private CompanyService service;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void init() {
        modelMapper.addMappings(new CompanyMap());
        modelMapper.addMappings(new FreeCompanyMap());
        modelMapper.addMappings(new PartnerMap());
        modelMapper.addMappings(new TapeMessageMap());
    }

    @RequestMapping(value = "/freeCompanies", method = RequestMethod.GET)
    public List<FreeCompanyDTO> getFreeCompanies() {
        List<FreeCompanyDTO> list = new ArrayList<>();
        List<Company> companies = service.getFreeCompanies();
        for (Company company : companies) {
            list.add(convertToFreeCompanyDTO(company));
        }

        return list;
    }

    @RequestMapping(value = "/company/{inn}", method = RequestMethod.GET)
    public CompanyDTO getCompanyByINN(@PathVariable("inn") Long inn) {
        context.setAttribute("company_inn", inn);
        return convertToCompanyDTO(service.getByINN(inn));
    }

    @RequestMapping(value = "/compatibleCompanies", method = RequestMethod.GET)
    public List<PartnerDTO> getCompatibleCompanies() {
        Long companyInn = (Long)context.getAttribute("company_inn");
        List<PartnerDTO> list = new ArrayList<>();
        List<Company> companies = service.getCompatibleCompanies(companyInn);
        for (Company company : companies) {
            list.add(convertToPartnerDTO(company));
        }

        return list;
    }

    @RequestMapping(value = "/personalCompanies", method = RequestMethod.GET)
    public List<PersonalCompanyDTO> getCompanyByOwner() {
        Long businessmanId = (Long)context.getAttribute("businessman_id");
        List<PersonalCompanyDTO> list = new ArrayList<>();
        List<Company> companies = service.getCompanyByOwner(businessmanId);
        list.addAll(companies.stream().map(this::convertToPersonalCompanyDTO).collect(Collectors.toList()));

        return list;
    }

    @RequestMapping(value = "/tapeCompanyMessages/{inn}", method = RequestMethod.GET)
    public TapeDTO getTapeCompanyMessages(@PathVariable("inn") Long inn) {
        Company company = service.getByINN(inn);

        TapeDTO tape = new TapeDTO();
        tape.setTitle(company.getName());
        tape.setIcon(company.getLogo());

        List<TapeMessage> messages = service.getMessagesByCompanyINN(inn);
        List<TapeMessageDTO> messagesDTO =
                messages.stream().map(this::convertToTapeMessageDTO).collect(Collectors.toList());
        tape.setMessages(messagesDTO);

        return tape;
    }

    private CompanyDTO convertToCompanyDTO(Company company) {
        return modelMapper.map(company, CompanyDTO.class);
    }

    private FreeCompanyDTO convertToFreeCompanyDTO(Company company) {
        return modelMapper.map(company, FreeCompanyDTO.class);
    }

    private PersonalCompanyDTO convertToPersonalCompanyDTO(Company company) {
        return modelMapper.map(company, PersonalCompanyDTO.class);
    }

    private PartnerDTO convertToPartnerDTO(Company company) {
        return modelMapper.map(company, PartnerDTO.class);
    }

    private TapeMessageDTO convertToTapeMessageDTO(TapeMessage tapeMessage) {
        return modelMapper.map(tapeMessage, TapeMessageDTO.class);
    }
}
