package net.ses.engine.controller.dto.good.mapping;


import net.ses.engine.controller.dto.good.ProductDTO;
import net.ses.engine.domain.Product;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 29.04.17.
 */
public class ProductMap extends PropertyMap<Product, ProductDTO> {

    @Override
    protected void configure() {
        map().setName(source.getGood().getName());
        map().setDescription(source.getGood().getDescription());
        map().setLogo(source.getGood().getLogo());
        map().setPrimeCost(source.getPrimeCost().getValue());
        map().setMarketCost(source.getMarketCost().getValue());
    }
}
