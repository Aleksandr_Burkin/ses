package net.ses.engine.controller.dto.tape;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by Aleksandr_Burkin on 5/12/2017.
 */
public class TapeMessageDTO {

    private String text;
    private String date;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(date);
        this.date = time;
    }

}
