package net.ses.engine.controller;

import net.ses.engine.controller.dto.good.ProductDTO;
import net.ses.engine.controller.dto.good.mapping.ProductMap;
import net.ses.engine.core.Money;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.Product;
import net.ses.engine.service.CompanyService;
import net.ses.engine.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aleksandr_Burkin on 29.04.17.
 */
@RestController
@RequestMapping(value = "/api")
public class ProductController {

    @Autowired
    private ServletContext context;

    @Autowired
    private ProductService service;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SimpMessagingTemplate template;

    @PostConstruct
    public void init() {
        modelMapper.addMappings(new ProductMap());
    }

    @RequestMapping(value = "/products/{companyINN}", method = RequestMethod.GET)
    public List<ProductDTO> getProducts(@PathVariable("companyINN") Long companyINN) {
        List<Product> products = service.getByCompanyINN(companyINN);

        return products.stream().map(this::convertToProductDTO).collect(Collectors.toList());
    }

    @RequestMapping(value = "/personalProducts", method = RequestMethod.GET)
    public List<ProductDTO> getPersonalProducts() {
        Long inn = (Long)context.getAttribute("company_inn");

        List<Product> products = service.getByCompanyINN(inn);

        return products.stream().map(this::convertToProductDTO).collect(Collectors.toList());
    }

    @RequestMapping(value = "/buyProduct/{fromCompanyINN}/{productId}/{count}", method = RequestMethod.POST)
    private String buyProduct(@PathVariable("fromCompanyINN") Long fromCompanyINN,
                           @PathVariable("productId") Long productId,
                           @PathVariable("count") Long count) {
        Long companyINN = (Long) context.getAttribute("company_inn");

        service.buyProduct(companyINN, productId, count);

        Money budget = companyService.getBudget(companyINN);

        Company partner = companyService.getByINN(fromCompanyINN);
        Product product = service.getById(productId);

        String message = "Покупка у компании <span class='item-name'>'" + partner.getName() + "'</span> " +
                         "товара <span class='item-name'>'" +
                          product.getGood().getName().replaceFirst("\"", "&#171;").replaceFirst("\"", "&#187;") + "'</span> " +
                         "(<em>" + count + "</em> единиц) " +
                         "на сумму <span class='money'>" + product.getMarketCost().getValue() * count + " &#8381;</span>";
        companyService.saveMessage(companyINN, message);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(new Date());
        this.template.convertAndSend("/topic/tape", "{\"text\":\"" + message + "\", \"date\":\"" + time + "\"}");

        return "{\"budget\":" + budget.getValue() + "}";
    }

    @RequestMapping(value = "/changeMarketCost/{productId}//{marketCost}", method = RequestMethod.PUT)
    private void changeMarketCost(@PathVariable("productId") Long productId,
                                  @PathVariable("marketCost") Long marketCost) {
        service.changeMarketCost(productId, marketCost);

        Product product = service.getById(productId);
        String message = "Фиксация рыночной цены продукта <span class='item-name'>'" +
                         product.getGood().getName().replaceFirst("\"", "&#171;").replaceFirst("\"", "&#187;") + "'</span> " +
                         "в размере <span class='money'>" + product.getMarketCost().getValue() + " &#8381;</span>";
        Long companyINN = (Long) context.getAttribute("company_inn");
        companyService.saveMessage(companyINN, message);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(new Date());
        this.template.convertAndSend("/topic/tape", "{\"text\":\"" + message + "\", \"date\":\"" + time + "\"}");
    }

    private ProductDTO convertToProductDTO(Product product) {
        return modelMapper.map(product, ProductDTO.class);
    }
}
