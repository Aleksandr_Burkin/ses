package net.ses.engine.controller.dto.good.mapping;

import net.ses.engine.controller.dto.good.AssetDTO;
import net.ses.engine.domain.Asset;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 5/12/2017.
 */
public class AssetMap extends PropertyMap<Asset, AssetDTO> {

    @Override
    protected void configure() {
        map().setName(source.getGood().getName());
        map().setDescription(source.getGood().getDescription());
        map().setLogo(source.getGood().getLogo());
        map().setPrimeCost(source.getPrimeCost().getValue());
        map().setOwner(source.getGood().getCompany().getName());
    }
}
