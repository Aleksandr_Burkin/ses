package net.ses.engine.controller.dto.company.mapping;

import net.ses.engine.controller.dto.company.FreeCompanyDTO;
import net.ses.engine.domain.Company;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 11.05.17.
 */
public class FreeCompanyMap extends PropertyMap<Company, FreeCompanyDTO> {

    @Override
    protected void configure() {
        map().setEconomicType(source.getEconomicType().getDescription());
        map().setPrice(source.getPrice().getValue());
    }
}
