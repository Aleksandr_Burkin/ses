package net.ses.engine.controller.dto.company.mapping;

import net.ses.engine.controller.dto.company.PartnerDTO;
import net.ses.engine.domain.Company;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 29.04.17.
 */
public class PartnerMap extends PropertyMap<Company, PartnerDTO> {
    @Override
    protected void configure() {
        map().setEconomicType(source.getEconomicType().getDescription());
    }
}
