package net.ses.engine.controller.dto.tape;

import java.util.List;

/**
 * Created by Aleksandr_Burkin on 09.05.17.
 */
public class TapeDTO {

    private String title;

    private String icon;

    private List<TapeMessageDTO> messages;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<TapeMessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<TapeMessageDTO> messages) {
        this.messages = messages;
    }
}
