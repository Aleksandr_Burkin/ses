package net.ses.engine.controller.dto.company;

/**
 * Created by Aleksandr_Burkin on 3/2/2017.
 */
public class PersonalCompanyDTO {

    private Long INN;
    private String name;
    private String logo;

    public Long getINN() {
        return INN;
    }

    public void setINN(Long INN) {
        this.INN = INN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
