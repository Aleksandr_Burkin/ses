package net.ses.engine.controller.dto.company.mapping;

import net.ses.engine.controller.dto.company.CompanyDTO;
import net.ses.engine.domain.Company;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 11.05.17.
 */
public class CompanyMap extends PropertyMap<Company, CompanyDTO> {

    @Override
    protected void configure() {
        map().setEconomicType(source.getEconomicType().getDescription());
        map().setBudget(source.getBudget().getValue());
    }
}
