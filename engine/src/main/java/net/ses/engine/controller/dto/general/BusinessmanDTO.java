package net.ses.engine.controller.dto.general;

/**
 * Created by Aleksandr_Burkin on 3/2/2017.
 */
public class BusinessmanDTO {

    private String firstName;
    private String lastName;
    private String avatar;
    private Long cash;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getCash() {
        return cash;
    }

    public void setCash(Long cash) {
        this.cash = cash;
    }
}
