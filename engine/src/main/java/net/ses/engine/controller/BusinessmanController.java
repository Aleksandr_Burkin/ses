package net.ses.engine.controller;

import net.ses.engine.controller.dto.general.BusinessmanDTO;
import net.ses.engine.controller.dto.tape.TapeDTO;
import net.ses.engine.controller.dto.general.mapping.BusinessmanMap;
import net.ses.engine.controller.dto.tape.TapeMessageDTO;
import net.ses.engine.controller.dto.tape.mapping.TapeMessageMap;
import net.ses.engine.core.Money;
import net.ses.engine.domain.Businessman;
import net.ses.engine.domain.Company;
import net.ses.engine.domain.TapeMessage;
import net.ses.engine.security.auth.JwtAuthenticationToken;
import net.ses.engine.security.model.UserContext;
import net.ses.engine.service.BusinessmanService;
import net.ses.engine.service.CompanyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by Aleksandr_Burkin on 3/2/2017.
 */
@RestController
@RequestMapping(value = "/api")
public class BusinessmanController {

    @Autowired
    private ServletContext context;

    @Autowired
    private BusinessmanService service;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SimpMessagingTemplate template;

    @Resource(name = "tapeMessages")
    private Properties tapeMessages;

    @PostConstruct
    public void init() {
        modelMapper.addMappings(new BusinessmanMap());
        modelMapper.addMappings(new TapeMessageMap());
    }

    @RequestMapping(value = "/businessmanInfo", method = RequestMethod.GET)
    public BusinessmanDTO getBusinessmanById() {
        return convertToBusinessmanDTO(service.getBusinessmanById((Long)context.getAttribute("businessman_id")));
    }

    @RequestMapping(value = "/buyCompany/{companyINN}", method = RequestMethod.PUT)
    public String buyCompany(@PathVariable("companyINN") Long companyINN) {
        Long businessmanId = (Long)context.getAttribute("businessman_id");
        service.buyCompany(businessmanId, companyINN);
        Money cash = service.getCash(businessmanId);

        Company company = companyService.getByINN(companyINN);

        String businessmanProperty = (String)tapeMessages.get("businessman.buy.company");
        String businessmanMessage = MessageFormat.format(businessmanProperty, company.getName(), company.getPrice().getValue());
        String companyProperty = (String)tapeMessages.get("company.buy.company");
        String companyMessage = MessageFormat.format(companyProperty, company.getPrice().getValue());

        //TODO вынести общую дату
        companyService.saveMessage(companyINN, companyMessage);
        service.saveMessage(businessmanId, businessmanMessage);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(new Date());
        this.template.convertAndSend("/topic/tape", "{\"text\":\"" + businessmanMessage + "\", \"date\":\"" + time + "\"}");

        return "{\"cash\":" + cash.getValue() + "}";
    }

    @RequestMapping(value = "/invest/{amount}", method = RequestMethod.PUT)
    public String investToCompany(@PathVariable("amount") Long amount) {
        Long businessmanId = (Long)context.getAttribute("businessman_id");
        Long companyINN = (Long)context.getAttribute("company_inn");
        service.investToCompany(businessmanId, companyINN, new Money(amount));
        Money cash = service.getCash(businessmanId);
        Money budget = companyService.getBudget(companyINN);

        Company company = companyService.getByINN(companyINN);

        String businessmanProperty = (String)tapeMessages.get("businessman.invest");
        String businessmanMessage = MessageFormat.format(businessmanProperty, company.getName(), amount);
        String companyProperty = (String)tapeMessages.get("company.invest");
        String companyMessage = MessageFormat.format(companyProperty, amount);

        companyService.saveMessage(companyINN, companyMessage);
        service.saveMessage(businessmanId, businessmanMessage);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(new Date());
        this.template.convertAndSend("/topic/tape", "{\"text\":\"" + companyMessage + "\", \"date\":\"" + time + "\"}");

        return "{\"cash\":" + cash.getValue() +
               ",\"budget\":" + budget.getValue() + "}";
    }

    @RequestMapping(value = "/tapeBusinessmanMessages", method = RequestMethod.GET)
    public TapeDTO getTapeBusinessmanMessages() {
        Long businessmanId = (Long)context.getAttribute("businessman_id");

        TapeDTO tape = new TapeDTO();
        tape.setTitle("Лента");

        List<TapeMessage> messages = service.getMessagesByBusinessmanId(businessmanId);
        List<TapeMessageDTO> messagesDTO =
                messages.stream().map(this::convertToTapeMessageDTO).collect(Collectors.toList());
        tape.setMessages(messagesDTO);

        return tape;
    }

    /**
     * For test
     *
     * @param token
     * @return
     */
    @RequestMapping(value="/api/user", method=RequestMethod.GET)
    public @ResponseBody UserContext get(JwtAuthenticationToken token) {
        return (UserContext) token.getPrincipal();
    }

    private BusinessmanDTO convertToBusinessmanDTO(Businessman businessman) {
        return modelMapper.map(businessman, BusinessmanDTO.class);
    }

    private TapeMessageDTO convertToTapeMessageDTO(TapeMessage tapeMessage) {
        return modelMapper.map(tapeMessage, TapeMessageDTO.class);
    }
}
