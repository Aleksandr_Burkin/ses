package net.ses.engine.controller.dto.good;

/**
 * Created by Aleksandr_Burkin on 4/25/2017.
 */
public class ProductDTO {

    private Long id;
    private String name;
    private String description;
    private String logo;
    private Long primeCost;
    private Long marketCost;
    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Long getPrimeCost() {
        return primeCost;
    }

    public void setPrimeCost(Long primeCost) {
        this.primeCost = primeCost;
    }

    public Long getMarketCost() {
        return marketCost;
    }

    public void setMarketCost(Long marketCost) {
        this.marketCost = marketCost;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
