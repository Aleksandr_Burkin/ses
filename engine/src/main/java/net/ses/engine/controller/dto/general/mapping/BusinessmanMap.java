package net.ses.engine.controller.dto.general.mapping;

import net.ses.engine.controller.dto.general.BusinessmanDTO;
import net.ses.engine.domain.Businessman;
import org.modelmapper.PropertyMap;

/**
 * Created by Aleksandr_Burkin on 5/11/2017.
 */
public class BusinessmanMap extends PropertyMap<Businessman, BusinessmanDTO> {

    @Override
    protected void configure() {
        map().setCash(source.getCash().getValue());
    }
}
