package net.ses.engine.controller;

import net.ses.engine.controller.dto.general.mapping.BusinessmanMap;
import net.ses.engine.controller.dto.good.AssetDTO;
import net.ses.engine.controller.dto.good.ProductDTO;
import net.ses.engine.controller.dto.good.mapping.AssetMap;
import net.ses.engine.domain.Asset;
import net.ses.engine.domain.Product;
import net.ses.engine.service.AssetService;
import net.ses.engine.service.CompanyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aleksandr_Burkin on 27.04.17.
 */
@RestController
@RequestMapping(value = "/api")
public class AssetController {

    @Autowired
    private ServletContext context;

    @Autowired
    private AssetService service;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SimpMessagingTemplate template;

    @PostConstruct
    public void init() {
        modelMapper.addMappings(new AssetMap());
    }

    @RequestMapping(value = "/assets", method = RequestMethod.GET)
    public List<AssetDTO> getAssets() {
        Long companyInn = (Long)context.getAttribute("company_inn");
        List<Asset> assets = service.getByCompanyINN(companyInn);

        return assets.stream().map(this::convertToAssetDTO).collect(Collectors.toList());
    }

    @RequestMapping(value = "/intoProducts/{assetId}/{count}", method = RequestMethod.PUT)
    public String intoProducts(@PathVariable("assetId") Long assetId,
                              @PathVariable("count") Long count) {
        Long companyINN = (Long) context.getAttribute("company_inn");

        Asset asset = service.getById(assetId);
        Long newCount = service.intoProducts(companyINN, assetId, count);

        String message = "Перевод на торги актива <span class='item-name'>'" +
                          asset.getGood().getName().replaceFirst("\"", "&#171;").replaceFirst("\"", "&#187;") + "'</span> " +
                         "(<em>" + count + "</em> единиц)" ;
        companyService.saveMessage(companyINN, message);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time  = dateFormat.format(new Date());
        this.template.convertAndSend("/topic/tape", "{\"text\":\"" + message + "\", \"date\":\"" + time + "\"}");

        return "{\"count\":" + newCount + "}";
    }

    private AssetDTO convertToAssetDTO(Asset asset) {
        return modelMapper.map(asset, AssetDTO.class);
    }
}
