package net.ses.engine.exception;

/**
 * Created by Aleksandr_Burkin on 27.03.17.
 */
public class NotEnoughMoneyException extends RuntimeException {

    public NotEnoughMoneyException(String msg) {
        super(msg);
    }

    public NotEnoughMoneyException(String msg, Throwable t) {
        super(msg, t);
    }
}
