package net.ses.engine.exception;

/**
 * Created by Aleksandr_Burkin on 01.05.17.
 */
public class IncorrectCountValueException extends RuntimeException {

    public IncorrectCountValueException(String msg) {
        super(msg);
    }

    public IncorrectCountValueException(String msg, Throwable t) {
        super(msg, t);
    }
}
