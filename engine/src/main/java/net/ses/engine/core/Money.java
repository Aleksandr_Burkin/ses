package net.ses.engine.core;

import java.util.Currency;
import java.util.Objects;

/**
 * Created by Roman_Nasonov on 2/21/2017.
 *
 * Базовый immutable-класс для представления валюты
 */
public class  Money {

    private Long value;
    private static final String RUR = "RUR";
    private Currency currency = Currency.getInstance(RUR);

    public Money(Long value) {
        if( value < 0 )
            throw new IllegalArgumentException("Некорректная сумма денег");
        this.value = value;
    }

    public Money(Long value, Currency currency) {
        this(value);
        this.currency = currency;
    }

    public Long getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Money add(Money money) {
        return new Money(this.value + money.getValue());
    }

    public Money subtract(Money money) {
        if( this.value < money.getValue() )
            throw new IllegalArgumentException("Некорректная сумма денег");
        return new Money(this.value - money.getValue());
    }

    public Money multiply(Long multiplier) {
        return new Money(this.getValue() * multiplier);
    }

    @Override
    public boolean equals(Object obj) {
        if( obj == null || getClass() != obj.getClass() )
            return false;

        Money money = (Money)obj;

        return value.equals(money.getValue()) &&
               currency.equals(money.getCurrency());
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }

    @Override
    public String toString() {
        return value.toString() + " " + currency;
    }
}
