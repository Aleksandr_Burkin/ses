package net.ses.engine.core.cache;

/**
 * Created by Aleksandr_Burkin on 23.02.17.
 */
public class CacheDoesNotExistException extends RuntimeException {

    public CacheDoesNotExistException(String message) {
        super(message);
    }
}
