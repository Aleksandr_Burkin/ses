package net.ses.engine.core.cache;

import org.ehcache.Cache;

/**
 * Created by Aleksandr_Burkin on 23.02.17.
 */
public interface CacheRepository {

    Cache createCache(String name, Class key, Class value, Long size);

    Cache getCache(String name, Class key, Class value);

    void removeCache(String name);

    /**
     * инициализация
     */
    void init(String name);

    /**
     * освобождает ресурсы
     */
    void close(String name);
}
