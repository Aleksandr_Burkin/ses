package net.ses.engine.core.cache;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Aleksandr_Burkin on 23.02.17.
 *
 * Общее хранилище кэшей
 */
public class GeneralCacheRepository implements CacheRepository {

    private Map<String, CacheManager> managers;

    public GeneralCacheRepository() {
        this.managers = new HashMap<>();
    }

    @Override
    public Cache createCache(String name, Class key, Class value, Long size) {
        CacheConfigurationBuilder config = CacheConfigurationBuilder
                                          .newCacheConfigurationBuilder(key,
                                                                        value,
                                                                        ResourcePoolsBuilder.heap(size));

        CacheManagerBuilder<CacheManager> managerBuilder = CacheManagerBuilder.newCacheManagerBuilder();
        CacheManager cacheManager = managerBuilder.withCache(name, config).build(true);
        managers.put(name, cacheManager);

        return cacheManager.getCache(name, key, value);
    }

    @Override
    public Cache getCache(String name, Class key, Class value) {
        CacheManager cacheManager = getCacheManager(name);
        return cacheManager.getCache(name, key, value);
    }

    @Override
    public void removeCache(String name) {
        CacheManager cacheManager = getCacheManager(name);
        cacheManager.removeCache(name);
        managers.remove(name);
    }

    @Override
    public void init(String name) {
        CacheManager cacheManager = getCacheManager(name);
        cacheManager.init();
    }

    @Override
    public void close(String name) {
        CacheManager cacheManager = getCacheManager(name);
        cacheManager.close();
    }

    private CacheManager getCacheManager(String name) {
        CacheManager cacheManager = managers.get(name);
        if( !Objects.nonNull(cacheManager) )
            throw new CacheDoesNotExistException("Кэш с таким именем не существует");

        return cacheManager;
    }

}
