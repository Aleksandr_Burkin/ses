﻿При установке на windows скачать необходимый дистрибутив с https://nodejs.org/en/download/. При установке на linux ubuntu можно воспользоваться утилитой apt-get.

После установки node.js, так же будет установлен пакетный менеджер npm.

1) В необходимой папке (корень проект) выполнить: 
	npm init

2) далее выполнить последовательно команды ниже:
	npm install webpack --save
	npm install webpack-dev-server --save
	npm install react --save
	npm install react-dom --save
	npm i --save babel-core babel-plugin-transform-decorators-legacy babel-polyfill babel-preset-es2015 babel-preset-react babel-preset-stage-0 babel-loader
	npm i react-router --save
	npm install --save whatwg-fetch (polyfill для старых браузеров)
	npm install --save react-addons-update
	npm install react-addons-css-transition-group
	npm install react-alert
	npm i jquery --save
	npm install react-cookie --save
	npm install react-bootstrap-tabs --save

	В итоге будет сгненерирована папка node_modules в корне проекта.
	
3) Далее в корень проекта скопировать все файлы, лежащие в корне директории текущего файла (который вы читаете) . Это hello_world с примером rest-сервиса.

4) Запустить node командой npm start (так же должен быть запущен spring boot для работы rest-сервиса).