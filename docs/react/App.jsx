import React from 'react';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {businessman: {}};
	}

	componentDidMount() {
		fetch('http://localhost:8181/man/1')
		.then((response) => response.json())
		.then((responseJson) => {
		    this.setState({businessman: responseJson});
		});
	}

    render() {
        return (
            <Businessman man={this.state.businessman}/>
        );
    }
}

class Businessman extends React.Component {
	render() {
		return (
			<div>
				<div> {this.props.man.lastName} </div>
			</div>
		)
	}
}

export default App;